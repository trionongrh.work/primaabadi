-- Adminer 4.7.6 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `company_profile`;
CREATE TABLE `company_profile` (
  `key` varchar(255) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `company_profile` (`key`, `value`) VALUES
('cp_visi',	'<p>\r\n                        Menjadi Manufaktur Karoseri terintegrasi dan terkemuka di Indonesia.\r\n                    </p>'),
('cp_misi',	'<ul><li>Mengutamakan pelayanan dan kepuasan konsumen.</li><li> Efisiensi dan produktifitas dalam menjalankan produksi.</li><li>Pengembangan produk melalui SDM yang profesional, kreatif, inovatif, yang didukung oleh perangkat sistem engineering spesifikasi material prosedur kerja.</li><li>Membuat produk berkualitas sesuai dengan standar yang ditetapkan pemerintah.</li></ul>'),
('cp_phone',	'0811-9110-988'),
('cp_email',	'marketing@primausahamitraabadi.com'),
('cp_addr1',	'By Pass Jomin No. 88, Cikampek, Jawa Barat'),
('cp_addr2',	'Cikampek, Jawa Barat'),
('cp_about_title',	'WE ARE FOCUSED TO<br/>“QUALITY SATISFACTION”'),
('cp_about_line1',	'Kami PT. Prima Usaha Mitra Abadi adalah sebuah perusahaan berbadan hukum yang bergerak dalam bidang Karoseri, khususnya karoseri untuk kendaraan niaga serta juga bergerak dalam Bidang Perbengkelan Umum & Transportasi.'),
('cp_about_line2',	'Kami mempunyai motto kerja “QUALITY SATISFACTION” oleh karena itu kami senantiasa menjaga agar pekerjaan selesai tepat waktu dan melakukan kontrol kualitas secara berkesinambungan agar pelanggan puas atas hasil pekerjaan kami.'),
('cp_about_image',	'qs.jpg');

DROP TABLE IF EXISTS `company_profile_aboutlist`;
CREATE TABLE `company_profile_aboutlist` (
  `id_cp_about` int(11) NOT NULL AUTO_INCREMENT,
  `title_cp_about` varchar(255) NOT NULL,
  `image_cp_about` varchar(255) NOT NULL,
  `desc_cp_about` text NOT NULL,
  PRIMARY KEY (`id_cp_about`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `company_profile_aboutlist` (`id_cp_about`, `title_cp_about`, `image_cp_about`, `desc_cp_about`) VALUES
(1,	'QUALITY',	'card1.jpg',	'Membangun sudut pandang pelanggan ke dalam setiap aspek produk, mulai dari desain hingga hasil akhir yang berkualitas.'),
(2,	'SATISFACTION',	'card2.jpg',	'Kami senantiasa menjaga agar pekerjaan selesai tepat waktu sehingga pelanggan puas atas hasil kerja kami.'),
(3,	'INNOVATION',	'card3.jpg',	'Keberhasilan produk kami untuk terdaftar pada standart kendaraan bermotor menjadi cerminan dari inovasi produk yang bermutu.'),
(4,	'IMPRESSION',	'card4.jpg',	'Produk yang dihasilkan menampilkan kesan yang tak terlupakan dan sesuai dengan keinginan pelanggan.');

DROP TABLE IF EXISTS `company_profile_services`;
CREATE TABLE `company_profile_services` (
  `id_cp_services` int(11) NOT NULL AUTO_INCREMENT,
  `image_cp_services` varchar(255) NOT NULL,
  `title_cp_services` varchar(255) NOT NULL,
  `icon_cp_services` varchar(255) NOT NULL,
  `desc_cp_services` longtext NOT NULL,
  PRIMARY KEY (`id_cp_services`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `company_profile_services` (`id_cp_services`, `image_cp_services`, `title_cp_services`, `icon_cp_services`, `desc_cp_services`) VALUES
(4,	'caroserie.jpeg',	'CAROSERIE MODELING',	'im-pencil-ruler',	'Menyusun rangkaian keputusan dan tindakan yang akan dilakukan guna pelaksaan proyek yang dibatasi oleh ukuran-ukuran dalam aspek biaya, mutu dan waktu, untuk mencapai sasaran tertentu yang telah ditetapkan oleh perusahaan.'),
(5,	'custom.jpeg',	'CUSTOM MADE',	'im-wrench',	'Jenis model skala yang dibangun untuk mempelajari aspek-aspek desain arsitektur atau untuk mengkomunikasikan ide - ide desain. Tergantung pada tujuannya, model dapat dibuat dari berbagai bahan dan pada berbagai skala.'),
(6,	'maintenance.jpg',	'MAINTENANCE & REPAIR',	'im-gears',	'Layanan yang disediakan untuk seluruh proses konstruksi yang sangat baik. Sebagai inovasi terbaik dalam tahap konstruksi proyek anda dan tujuan penjadwalan yang didefinisikan dengan baik.');

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `config_key` varchar(255) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `config` (`config_key`, `value`) VALUES
('maintenanceMode',	'0');

DROP TABLE IF EXISTS `inbox`;
CREATE TABLE `inbox` (
  `id_inbox` int(11) NOT NULL AUTO_INCREMENT,
  `name_inbox` varchar(255) NOT NULL,
  `email_inbox` varchar(255) NOT NULL,
  `phone_inbox` varchar(255) NOT NULL,
  `message_inbox` longtext NOT NULL,
  `is_read` int(11) NOT NULL,
  `at` datetime NOT NULL,
  PRIMARY KEY (`id_inbox`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id_post` int(11) NOT NULL AUTO_INCREMENT,
  `title_post` varchar(255) NOT NULL,
  `content_post` longtext NOT NULL,
  `time_post` datetime NOT NULL,
  `isPublish` int(11) NOT NULL,
  PRIMARY KEY (`id_post`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `post_log`;
CREATE TABLE `post_log` (
  `id_post_log` int(11) NOT NULL AUTO_INCREMENT,
  `status_post_log` enum('add','edit','delete') NOT NULL,
  `id_post` int(11) NOT NULL,
  `old_post_data` longtext NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_post_log`),
  KEY `id_post` (`id_post`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `post_log_ibfk_1` FOREIGN KEY (`id_post`) REFERENCES `post` (`id_post`),
  CONSTRAINT `post_log_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `nama_product` varchar(255) NOT NULL,
  `desc_product` text NOT NULL,
  `image_product` varchar(255) NOT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `product` (`id_product`, `nama_product`, `desc_product`, `image_product`) VALUES
(1,	'Tanki Standart Pertamina 16.000L ',	'<p>Testing</p>',	'1_1.jpg'),
(2,	'Fuel Tank 10.000L Berau Coal',	'<p>Test</p>',	'1_2.jpg'),
(3,	'Tangki Kimia Semi Trailer Puma Trans Internusa',	'<p>Test</p>',	'1_3.jpg'),
(4,	'Tangki Pertamax Semi Trailer 24.000 L Patraniaga',	'<p>Test</p>',	'1_4.jpg'),
(5,	'Tangki Petronas 16.000 L',	'<p>Test</p>',	'1_5.jpg'),
(6,	'Tangki Semi Trailer SK Keris',	'<p>Test</p>',	'1_6.jpg'),
(7,	'Tangki Vacuum Hino',	'<p>Test</p>',	'1_7.jpg'),
(8,	'Dump Truck Aero',	'<p>Test</p>',	'2_1.jpg'),
(9,	'Dump Truck Kotak',	'<p>Test</p>',	'2_2.jpg'),
(10,	'Dump Truck Scoop End',	'<p>Test</p>',	'2_3.jpg'),
(11,	'Dump Truck Telescopic',	'<p>Test</p>',	'2_4.jpg'),
(12,	'Dump Truck Tronton',	'<p>Test</p>',	'2_5.jpg'),
(13,	'Car Carrier',	'<p>Test</p>',	'3_1.jpg'),
(14,	'Logging',	'<p>Tesst</p>',	'3_2.jpg'),
(15,	'Platform Cane dan Lift',	'<p>Test</p>',	'3_3.jpg'),
(16,	'Platform Crane',	'<p>Test</p>',	'3_4.jpg'),
(17,	'Platform Dinding Samping 4 Roda',	'<p>Test</p>',	'3_5.jpg'),
(18,	'Platform Logging',	'<p>Test</p>',	'3_6.jpg'),
(19,	'Self Loader',	'<p>Test</p>',	'3_7.jpg'),
(20,	'Semi Trailer',	'<p>Test</p>',	'3_8.jpg'),
(21,	'Hi Wing Hydraulic',	'<p>Test</p>',	'4_1.jpg'),
(22,	'Wing Box Allumunium Mobile Service',	'<p>Test</p>',	'4_2.jpg'),
(23,	'Wing Box Manual',	'<p>Test</p>',	'4_3.jpg'),
(24,	'Wing Box Mercy',	'<p>Test</p>',	'4_4.jpg'),
(25,	'Pertashop',	'<p>Test</p>',	'5_1.jpeg');

DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `id_product_category` int(11) NOT NULL AUTO_INCREMENT,
  `nama_product_category` varchar(255) NOT NULL,
  PRIMARY KEY (`id_product_category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `product_category` (`id_product_category`, `nama_product_category`) VALUES
(1,	'TANK'),
(2,	'DUMP TRUCK'),
(3,	'PLATFORM'),
(4,	'BOX'),
(5,	'SPECIAL ORDER');

DROP TABLE IF EXISTS `product_category_relations`;
CREATE TABLE `product_category_relations` (
  `id_product_category_relations` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `id_product_category` int(11) NOT NULL,
  PRIMARY KEY (`id_product_category_relations`),
  KEY `id_product` (`id_product`),
  KEY `id_product_category` (`id_product_category`),
  CONSTRAINT `product_category_relations_ibfk_3` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`) ON DELETE NO ACTION,
  CONSTRAINT `product_category_relations_ibfk_4` FOREIGN KEY (`id_product_category`) REFERENCES `product_category` (`id_product_category`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `product_category_relations` (`id_product_category_relations`, `id_product`, `id_product_category`) VALUES
(3,	3,	1),
(4,	4,	1),
(5,	5,	1),
(6,	6,	1),
(12,	7,	1),
(13,	8,	2),
(14,	9,	2),
(15,	10,	2),
(16,	11,	2),
(17,	12,	2),
(18,	13,	3),
(19,	14,	3),
(20,	15,	3),
(21,	16,	3),
(22,	17,	3),
(23,	18,	3),
(24,	19,	3),
(25,	20,	3),
(26,	21,	4),
(27,	22,	4),
(28,	23,	4),
(29,	24,	4),
(30,	25,	5),
(34,	2,	1),
(35,	1,	1);

DROP TABLE IF EXISTS `product_log`;
CREATE TABLE `product_log` (
  `id_product_log` int(11) NOT NULL AUTO_INCREMENT,
  `status_product_log` enum('add','edit','delete') NOT NULL,
  `id_product` int(11) NOT NULL,
  `old_product_data` text NOT NULL,
  PRIMARY KEY (`id_product_log`),
  KEY `id_product` (`id_product`),
  CONSTRAINT `product_log_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_level` int(11) NOT NULL,
  `nama_user` varchar(255) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id_user`, `username`, `password`, `user_level`, `nama_user`) VALUES
(1,	'admin',	'21232f297a57a5a743894a0e4a801fc3',	3,	'Admin');

DROP TABLE IF EXISTS `user_level`;
CREATE TABLE `user_level` (
  `user_level` int(11) NOT NULL,
  `name_level` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_level` (`user_level`, `name_level`) VALUES
(0,	'Guest'),
(1,	'Member'),
(2,	'Admin'),
(3,	'Superadmin');

DROP TABLE IF EXISTS `user_log`;
CREATE TABLE `user_log` (
  `id_user_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `status_user_log` enum('isLogin','isLogout') NOT NULL,
  `user_agent` longtext NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `log_at` datetime NOT NULL,
  PRIMARY KEY (`id_user_log`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `user_log_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_log` (`id_user_log`, `id_user`, `username`, `status_user_log`, `user_agent`, `ip_address`, `log_at`) VALUES
(1,	1,	'admin',	'isLogin',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',	'::1',	'2020-02-24 21:48:23'),
(2,	1,	'admin',	'isLogout',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',	'::1',	'2020-02-24 21:48:40'),
(3,	1,	'admin',	'isLogin',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',	'::1',	'2020-02-24 21:48:52'),
(4,	1,	'admin',	'isLogin',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',	'::1',	'2020-02-25 21:51:09'),
(5,	1,	'admin',	'isLogout',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',	'::1',	'2020-02-25 21:52:36'),
(6,	1,	'admin',	'isLogin',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',	'::1',	'2020-02-25 21:53:09'),
(7,	1,	'admin',	'isLogin',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',	'::1',	'2020-02-26 03:32:02'),
(8,	1,	'admin',	'isLogin',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',	'::1',	'2020-02-26 10:53:23'),
(9,	1,	'admin',	'isLogin',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',	'::1',	'2020-02-27 08:17:03'),
(10,	1,	'admin',	'isLogin',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36',	'::1',	'2020-02-28 07:45:46'),
(11,	1,	'admin',	'isLogin',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36',	'::1',	'2020-02-28 22:17:16'),
(12,	1,	'admin',	'isLogin',	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36',	'::1',	'2020-02-29 12:10:07');

-- 2020-02-29 06:23:56
