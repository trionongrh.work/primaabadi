<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function index()
	{
		$maintenance = $this->M_settings->getMaintenance();
		$data_content = array(
			'slider' => $this->M_company->getSlider(),
			'slider_config' => $this->M_company->getSliderConfig(),
			'visi' => $this->M_company->getVisi(),
			'misi' => $this->M_company->getMisi(),
			'services' => $this->M_company->getServices(),
			'contact' => $this->M_company->getContact(),
			'about' => $this->M_company->getAboutDetail(),
			'aboutlist' => $this->M_company->getAboutList(),
			'product_cat' => $this->M_product->getProductCategory(),
			'product' => $this->M_product->getProduct(),
			'setting' => $this->M_settings->getSettings()
		);
		if($maintenance){
			$this->load->view('maintenance',$data_content);
		}else{
			$this->load->view('home',$data_content);
		}
	}

}
