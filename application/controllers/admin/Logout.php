<?php

class Logout extends MY_AdminCore{

    
    public function index(){
        // print_r($this->session->userdata());
        // exit;
        $this->log_user($this->session->userdata('user')['id_user'],'isLogout');
        $this->session->sess_destroy();
        redirect('admin');
    }
}