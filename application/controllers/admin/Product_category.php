<?php


class Product_category extends MY_AdminCore
{

    public function index()
    {

        $data = [
            'header' => [
                'page' => 'admin_product',
                'sub_page' => 'product_category'
            ],
            'content' => [
                'category' => $this->M_product->getProductCategory(),
            ]
        ];

        $this->createView('admin/product/category', $data);
    }

    public function delete()
    {
        $get = $this->input->get();

        if (isset($get['category'])) {
            $category = $this->M_product->getProductCategoryItem($get['category']);
            if ($category) {
                if ($this->M_product->deleteProductCategory($get['category'])) {
                    $this->notif('Delete Product Category', 'Success !', 'success');
                } else {
                    $this->notif('Delete Product Category', 'Error !', 'error');
                }
            } else {
                $this->notif('404 Not Found', 'Product Category Not Found', 'error');
            }
        } else {
            $this->notif('404 Not Found', 'Product Category Not Found', 'error');
        }

        redirect('admin/product_category');
    }

    public function edit_process(){
        $post = $this->input->post();
        $data = array(
            'id_product_category' => $post['id_category'],
            'nama_product_category' => $post['category_name']
        );

        if($this->M_product->editProductCategory($data)){
            $this->notif('Edit Product Category', 'Success !', 'success');
        }else{
            $this->notif('Edit Product Category', 'Error !', 'error');
        }
        redirect('admin/product_category');
    }

    public function add_process()
    {
        $post = $this->input->post();

        $data = array(
            'nama_product_category' => $post['category_name']
        );

        if ($this->M_product->addProductCategory($data)) {
            $this->notif('Add Product Category', 'Success !', 'success');
        } else {
            $this->notif('Add Product Category', 'Error !', 'error');
        }
        redirect('admin/product_category');
    }

    public function ajax_get_category()
    {
        $get = $this->input->get();
        $response = array(
            'status' => false,
        );

        if (isset($get['category'])) {
            $category = $this->M_product->getProductCategoryItem($get['category']);
            if ($category) {
                $response['data'] = $category;
                $response['status'] = true;
                $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($response));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($response));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($response));
        }
    }
}
