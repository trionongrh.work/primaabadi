<?php

class Settings extends MY_AdminCore{

    public function index(){
        

        $data = [
            'header' => [
                'page' => 'admin_settings',
                'sub_page' => 'settings_general'
            ],
            'content' => [
                'config' => $this->M_settings->getSettings()
            ]
        ];
        
        $this->createView('admin/settings/main',$data);
    }

    public function update_config(){
        $post = $this->input->post();
        $oldConfig = $this->M_settings->getSettings(true);


        $data = [];
        
        if(isset($post['maintenanceMode'])){
            unset($post['maintenanceMode']);
            $data[] = [
                'config_key' => 'maintenanceMode',
                'value' => '1'
            ];
        }else{
            $data[] = [
                'config_key' => 'maintenanceMode',
                'value' => '0'
            ];
        }

        foreach($post as $i => $val){
            $data[] = [
                'config_key' => $i,
                'value' => $val
            ];
        }

        $path = './uploads/company/';
        // remove old images
        if($_FILES['imagevisi']['name'] != ''){
            if(file_exists($path.$oldConfig['home_visimisi_img'])){
                unlink($path.$oldConfig['home_visimisi_img']);
            }
            $data[] = [
                'config_key' => 'home_visimisi_img',
                'value' => $this->uploadFile('imagevisi','company',TRUE,['height' => 1080, 'width' => 1920]),
            ];
        }
        if($_FILES['imagecontact']['name'] != ''){
            if(file_exists($path.$oldConfig['home_contact_img'])){
                unlink($path.$oldConfig['home_contact_img']);
            }
            $data[] = [
                'config_key' => 'home_contact_img',
                'value' => $this->uploadFile('imagecontact','company',TRUE,['height' => 1080, 'width' => 1920]),
            ];
        }

        if($this->M_settings->setConfig($data)){
            $this->notif('Update Settings','Success !','success');
        }else{
            $this->notif('Update Settings','Error !','error');
        }
        redirect('admin/settings');
    }
}