<?php


class Slider_home extends MY_AdminCore
{

    public function index()
    {
        $data = [
            'header' => [
                'page' => 'admin_company',
                'sub_page' => 'company_slider'
            ],
            'content' => [
                'config' => $this->M_company->getSliderConfig(),
                'slider' => $this->M_company->getSlider()
            ]
        ];

        $this->createView('admin/compro/slider/main', $data);
    }

    public function add_process()
    {
        $this->uploadConfig('slider');

        if ($this->upload->do_upload('imagefile')) {
            $filename = $this->upload->data('file_name');
            $resizer = $this->resizeImage('./uploads/slider/' . $filename, ['height' => 720, 'width' => 1280]);
        } else {
            $filename = 'empty.jpg';
        }

        $data = [
            'image_slider' => $filename,
            'is_active' => 1
        ];

        if ($this->M_company->addSlider($data)) {
            $this->notif('Add Slider', 'Success !', 'success');
        } else {
            $this->notif('Add Slider', 'Error !', 'error');
        }


        redirect('admin/slider_home');
    }

    public function delete($id)
    {
        $slider = $this->M_company->getSliderId($id);
        $path = './uploads/slider/' . $slider['image_slider'];
        if (file_exists($path)) {
            unlink($path);
        }


        if ($this->M_company->deleteSlider($id)) {
            $this->notif('Delete Slider', 'Success !', 'success');
        } else {
            $this->notif('Delete Slider', 'Error !', 'error');
        }
        redirect('admin/slider_home');
    }

    public function update_config()
    {
        $post = $this->input->post();
        $data = [
            isset($post['sliderControlNav']) ? ['key' => 'sliderControlNav', 'value' => '1'] : ['key' => 'sliderControlNav', 'value' => '0'],
            isset($post['sliderControlArrow']) ? ['key' => 'sliderControlArrow', 'value' => '1'] : ['key' => 'sliderControlArrow', 'value' => '0'],
            [
                'key' => 'sliderSpeed',
                'value' => $post['sliderSpeed']
            ],
        ];

        if ($this->M_company->setSliderConfig($data)) {
            $this->notif('Update Slider Configuration', 'Success !', 'success');
        } else {
            $this->notif('Update Slider Configuration', 'Error !', 'error');
        }
        redirect('admin/slider_home');
    }
}
