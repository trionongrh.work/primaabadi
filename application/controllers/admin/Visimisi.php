<?php

class Visimisi extends MY_AdminCore
{
    public function index()
    {
        $visi = $this->M_company->getVisi();
        $misi = $this->M_company->getMisi();

        $data = [
            'header' => [
                'page' => 'admin_company',
                'sub_page' => 'company_visimisi'
            ],
            'content' => [
                'visi' => $visi,
                'misi' => $misi
            ]
        ];
        
        $this->createView('admin/compro/visimisi/main',$data);
    }

    public function editvisi(){
        $post = $this->input->post();

        // echo '<pre>';
        // print_r($post);
        if($this->M_company->editvisi($post['form_visi'])){
            redirect('admin/visimisi');
        }
    }
    public function editmisi(){
        $post = $this->input->post();

        // echo '<pre>';
        // print_r($post);
        if($this->M_company->editmisi($post['form_misi'])){
            redirect('admin/visimisi');
        }
    }
}
