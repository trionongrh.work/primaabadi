<?php


class Product extends MY_AdminCore
{

    public function index()
    {

        $data = [
            'header' => [
                'page' => 'admin_product',
                'sub_page' => 'product_main'
            ],
            'content' => [
                'product' => $this->M_product->getProduct()
            ]
        ];

        $this->createView('admin/product/main', $data);
    }

    public function add()
    {

        $data = [
            'header' => [
                'page' => 'admin_product',
                'sub_page' => 'product_add'
            ],
            'content' => [
                'category' => $this->M_product->getProductCategory()
            ]
        ];

        $this->createView('admin/product/add', $data);
    }

    public function edit(){
        $get = $this->input->get();


        if(isset($get['product'])){
            $product = $this->M_product->getProductOne($get['product']);
            if($product){
                $data = [
                    'header' => [
                        'page' => 'admin_product',
                        'sub_page' => 'product_edit'
                    ],
                    'content' => [
                        'product' => $product,
                        'category' => $this->M_product->getProductCategory()
                    ]
                ];
                
                $this->createView('admin/product/edit',$data);
            }else{
                $this->notif('404 Not Found','Product not found','error');
                redirect('admin/product');
            }
        }else{
            $this->notif('404 Not Found','Product not found','error');
            redirect('admin/product');
        }

    }

    public function delete(){
        $get = $this->input->get();


        if(isset($get['product'])){
            $product = $this->M_product->getProductOne($get['product']);
            if($product){
                if($this->M_product->deleteProduct($get['product'])){
                    $image = explode('.',$product['image_product']);
                    unlink('./uploads/product/'.$product['image_product']);
                    unlink('./uploads/product/'.$image[0].'_thumb.'.$image[1]);
                    $this->notif('Delete Product','Success !','success');
                }else{
                    $this->notif('Delete Product','Error !','success');
                }
            }else{
                $this->notif('404 Not Found','Product not found','error');
            }
        }else{
            $this->notif('404 Not Found','Product not found','error');
        }
        redirect('admin/product');
    }

    public function add_process()
    {
        $post = $this->input->post();
        $this->uploadConfig('product');

        if ($this->upload->do_upload('imagefile')) {
            $filename = $this->upload->data('file_name');
            $this->createThumb('product/'.$filename);
        } else {
            $filename = 'empty.jpg';
        }

        $data_product = array(
            'nama_product' => $post['name-product'],
            'desc_product' => $post['desc-product'],
            'image_product' => $filename,
        );

        if ($this->M_product->addProduct($data_product)) {
            $id_product = $this->M_product->addProductId();

            $data_category = [];
            foreach ($post['category'] as $key) {
                $data_category[] = [
                    'id_product' => $id_product,
                    'id_product_category' => $key
                ];
            }

            if ($this->M_product->addProductCategortRelations($data_category)) {
                $this->notif('Add Product', 'Success !', 'success');
            }else{
                $this->notif('Add Product', 'Error !', 'success');
            }
        }else{
            $this->notif('Add Product', 'Error !', 'success');
        }

        redirect('admin/product');
    }

    public function edit_process(){
        $post = $this->input->post();
        $product = $this->M_product->getProductOne($post['id']);

        if($_FILES['imagefile']['name'] != ""){
            $image = explode('.',$product['image_product']);
            unlink('./uploads/product/'.$product['image_product']);
            unlink('./uploads/product/'.$image[0].'_thumb.'.$image[1]);
            $this->uploadConfig('product');

            if($this->upload->do_upload('imagefile')){
                $filename = $this->upload->data('file_name');
                $this->createThumb('product/'.$filename);
            }else{
                $filename = 'empty.jpg';
            }
        }else{
            $filename = $product['image_product'];
        }

        $data_product = array(
            'id_product' => $post['id'],
            'nama_product' => $post['name-product'],
            'desc_product' => $post['desc-product'],
            'image_product' => $filename,
        );
        

        $data_category = [];
        foreach ($post['category'] as $key) {
            $data_category[] = [
                'id_product' => $post['id'],
                'id_product_category' => $key
            ];
        }

        if($this->M_product->editProduct($data_product,$data_category)){
            $this->notif('Edit Product','Success !','success');
        }

        redirect('admin/product');
    }
}
