<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$session = $this->session->userdata('user');
		if($session){
			redirect('admin/home');
		}
	}

	public function index()
	{
		$post = $this->input->post();

		if(sizeof($post) > 0){
			$login_data = $this->M_user->getLogin($post['username'],$post['password']);

			if($login_data){

				// echo '<pre>';
				// print_r($login_data);
				// exit;
				$this->log_user($login_data['id_user'],'isLogin');
				$this->session->set_userdata(array('user' => $login_data));
				redirect('admin/home');
			}else{
				redirect('admin');
			}
		}else{
			$data_content = array(
				'contact' => $this->M_company->getContact()
			);
			$this->load->view('admin/login',$data_content);
		}
	}

	
    public function log_user($id_user, $status){
        $this->db->where('id_user',$id_user);
        $user = $this->db->get('user')->row_array();
        
        $data = array(
            'id_user' => $user['id_user'],
            'username' => $user['username'],
            'status_user_log' => $status,
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'log_at' => date('Y-m-d H:i:s')
        );

        $this->db->insert('user_log', $data);
    }
}
