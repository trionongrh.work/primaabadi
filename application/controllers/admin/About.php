<?php


class About extends MY_AdminCore {

    public function index(){

        $data = [
            'header' => [
                'page' => 'admin_company',
                'sub_page' => 'company_aboutus'
            ],
            'content' => [
                'aboutdetail' => $this->M_company->getAboutDetail(),
                'aboutlist' => $this->M_company->getAboutList()
            ]
        ];
        
        $this->createView('admin/compro/aboutus/main',$data);
    }

    public function add(){

        $data = [
            'header' => [
                'page' => 'admin_company',
                'sub_page' => 'company_aboutus'
            ],
        ];
        
        $this->createView('admin/compro/aboutus/add',$data);
    }

    public function edit(){
        $get = $this->input->get();

        if(isset($get['about'])){
            $about = $this->M_company->getAboutListItem($get['about']);
            if($about){
                $data = [
                    'header' => [
                        'page' => 'admin_company',
                        'sub_page' => 'company_aboutus'
                    ],
                    'content' => [
                        'aboutlist' => $about,
                    ]
                ];
                
                $this->createView('admin/compro/aboutus/edit',$data);
            }else{
                $this->notif('404 Not Found','About List not found','error');
                redirect('admin/about');
            }
        }else{
            $this->notif('404 Not Found','About List not found','error');
            redirect('admin/about');
        }

    }

    public function add_process(){
        $post = $this->input->post();
        $this->uploadConfig('about');

        if($this->upload->do_upload('imagefile')){
            $filename = $this->upload->data('file_name');
        }else{
            $filename = 'empty.jpg';
        }

        $data = array(
            'image_cp_about' => $filename,
            'title_cp_about' => $post['title'],
            'desc_cp_about' => $post['description']
        );
        
        if($this->M_company->addAboutList($data)){
            $this->notif('Add About List','Success !','success');
        }

        redirect('admin/about');
    }

    public function edit_process(){
        $post = $this->input->post();
        $about = $this->M_company->getAboutListItem($post['id']);

        if($_FILES['imagefile']['name'] != ""){
            unlink('./uploads/about/'.$about['image_cp_about']);
            $this->uploadConfig('about');

            if($this->upload->do_upload('imagefile')){
                $filename = $this->upload->data('file_name');
            }else{
                $filename = 'empty.jpg';
            }
        }else{
            $filename = $about['image_cp_about'];
        }
        
        $data = array(
            'id_cp_about' => $post['id'],
            'image_cp_about' => $filename,
            'title_cp_about' => $post['title'],
            'desc_cp_about' => $post['description']
        );

        if($this->M_company->editAboutList($data)){
            $this->notif('Edit About List','Success !','success');
        }

        redirect('admin/about');
    }

    public function delete(){
        $get = $this->input->get();

        if(isset($get['about'])){
            $service = $this->M_company->getAboutListItem($get['about']);
            if($service){
                if($this->M_company->deleteAboutList($get['about'])){
                    unlink('./uploads/about/'.$service['image_cp_about']);
                    $this->notif('Delete About List','Success !','success');
                }else{
                    $this->notif('Delete About List','Error !','error');
                }
            }else{
                $this->notif('404 Not Found','About List not found','error');
            }
        }else{
            $this->notif('404 Not Found','About List not found','error');
        }
        
        redirect('admin/about');
    }

    public function editdetail(){
        $post = $this->input->post();
        $about = $this->M_company->getAboutDetail();

        if($_FILES['imagefile']['name'] != ""){
            unlink('./uploads/services/'.$about['cp_about_image']);
            $this->uploadConfig('about');

            if($this->upload->do_upload('imagefile')){
                $filename = $this->upload->data('file_name');
            }else{
                $filename = 'empty.jpg';
            }
        }else{
            $filename = $about['cp_about_image'];
        }
        
        $data = array(
            [ 'key' => 'cp_about_image' , 'value' => $filename],
            [ 'key' => 'cp_about_title' , 'value' => $post['about-title']],
            [ 'key' => 'cp_about_line1' , 'value' => $post['about-line1']],
            [ 'key' => 'cp_about_line2' , 'value' => $post['about-line2']],
        );
        
        
        if($this->M_company->editCompany($data)){
            $this->notif('Edit About Detail', 'Success !', 'success');
        }else{
            $this->notif('Edit About Detail', 'Error !', 'error');
        }
        redirect('admin/about');
    }
}