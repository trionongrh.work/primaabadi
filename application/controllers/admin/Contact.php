<?php

class Contact extends MY_AdminCore {

    public function index(){
        $data = [
            'header' => [
                'page' => 'admin_company',
                'sub_page' => 'company_contact'
            ],
            'content' => [
                'contact' => $this->M_company->getContact(),
                'mail' => $this->M_mail->getMail(),
                'mail_size' => $this->M_mail->getMailSize(),
            ]
        ];
        
        $this->createView('admin/compro/contact/main',$data);
    }

    public function mail(){
        $get = $this->input->get();
        
        $data = [
            'header' => [
                'page' => 'admin_company',
                'sub_page' => 'company_contact'
            ],
            'content' => [
                'contact' => $this->M_company->getContact(),
            ]
        ];

        if(isset($get['mail'])){
            $data['content']['mail'] = $this->M_mail->getMailDetail($get['mail']);
            if(!$data['content']['mail']['is_read']){
                $this->M_mail->setRead($get['mail']);
            }
        }else{
            
        }
        
        $this->createView('admin/compro/contact/mail',$data);
    }

    public function maildelete(){
        $get = $this->input->get();
        if($this->M_mail->deleteMail($get['mail'])){
            $this->notif('Delete Mail','Success','success');
        }
        redirect('admin/contact');
    }

    public function updateContact(){
        $post = $this->input->post();
        $data = [];
        foreach($post as $i => $key){
            $data[] = array(
                'key' => $i,
                'value' => $key
            );
        }
        if($this->M_company->editContact($data)){

        }else{
            
        }
        redirect('admin/contact');
    }
}