<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_AdminCore {

	public function index()
	{
        $data = [
            'header' => [
                'page' => 'admin_home',
            ],
        ];
        $this->createView('admin/home',$data);
	}
}
