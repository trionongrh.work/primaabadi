<?php

class Services extends MY_AdminCore {

    public function index(){

        $data = [
            'header' => [
                'page' => 'admin_company',
                'sub_page' => 'company_services'
            ],
            'content' => [
                'services' => $this->M_company->getServices()
            ]
        ];

        $this->createView('admin/compro/services/main',$data);
    }

    
    public function add(){

        $data = [
            'header' => [
                'page' => 'admin_company',
                'sub_page' => 'company_services'
            ],
        ];
        
        $this->createView('admin/compro/services/add',$data);
    }

    public function edit(){
        $get = $this->input->get();

        if(isset($get['services'])){
            $service = $this->M_company->getService($get['services']);
            if($service){
                $data = [
                    'header' => [
                        'page' => 'admin_company',
                        'sub_page' => 'company_services'
                    ],
                    'content' => [
                        'services' => $service
                    ]
                ];
                
                $this->createView('admin/compro/services/edit',$data);
            }else{
                $this->notif('404 Not Found','Services not found','error');
                redirect('admin/services');
            }
        }else{
            $this->notif('404 Not Found','Services not found','error');
            redirect('admin/services');
        }
    }

    public function add_process(){
        $post = $this->input->post();
        $this->uploadConfig('services');

        if($this->upload->do_upload('imagefile')){
            $filename = $this->upload->data('file_name');
        }else{
            $filename = 'empty.jpg';
        }

        $data = array(
            'image_cp_services' => $filename,
            'title_cp_services' => $post['title'],
            'icon_cp_services' => $post['icon'],
            'desc_cp_services' => $post['description']
        );
        
        if($this->M_company->addServices($data)){
            $this->notif('Add Services','Success !','success');
        }

        redirect('admin/services');
    }

    public function edit_process(){
        $post = $this->input->post();
        $service = $this->M_company->getService($post['id']);

        if($_FILES['imagefile']['name'] != ""){
            unlink('./uploads/services/'.$service['image_cp_services']);
            $this->uploadConfig('services');

            if($this->upload->do_upload('imagefile')){
                $filename = $this->upload->data('file_name');
            }else{
                $filename = 'empty.jpg';
            }
        }else{
            $filename = $service['image_cp_services'];
        }
        
        $data = array(
            'id_cp_services' => $post['id'],
            'image_cp_services' => $filename,
            'title_cp_services' => $post['title'],
            'icon_cp_services' => $post['icon'],
            'desc_cp_services' => $post['description']
        );

        if($this->M_company->editServices($data)){
            $this->notif('Edit Services','Success !','success');
        }

        redirect('admin/services');
    }

    public function delete(){
        $get = $this->input->get();

        if(isset($get['services'])){
            $service = $this->M_company->getService($get['services']);
            if($service){
                if($this->M_company->deleteServices($get['services'])){
                    unlink('./uploads/services/'.$service['image_cp_services']);
                    $this->notif('Delete Services','Success !','success');
                }else{
                    $this->notif('Delete Services','Error !','error');
                }
            }else{
                $this->notif('404 Not Found','Services not found','error');
            }
        }else{
            $this->notif('404 Not Found','Services not found','error');
        }
        
        redirect('admin/services');
    }
}