<?php


class Product extends CI_Controller
{

	public function detail($id_product)
	{
		$maintenance = $this->M_settings->getMaintenance();
		$data_content = array(
			'contact' => $this->M_company->getContact(),
			'product' => $this->M_product->getProductOne($id_product),
			'product_cat' => $this->M_product->getProductCategory(),
		);

		$data_content['next'] = $this->M_product->getProductNext($id_product,$data_content['product']['category'][0]['id_product_category']);
		$data_content['prev'] = $this->M_product->getProductPrev($id_product,$data_content['product']['category'][0]['id_product_category']);
		// echo '<pre>';
		// echo json_encode($data_content);
		// exit;
		if ($maintenance) {
			redirect('home');
		} else {
			$this->load->view('product', $data_content);
		}
	}

	public function category($filter_key = '')
	{
		$maintenance = $this->M_settings->getMaintenance();
		$data_content = array(
			'contact' => $this->M_company->getContact(),
			'setting' => $this->M_settings->getSettings(),
			'product_cat' => $this->M_product->getCategoryWithProduct(),
			'itemPerpage' => 8,
			'activekey' => $filter_key,
		);
		// echo '<pre>';
		// echo json_encode($data_content['product_cat']);
		// exit;
		if ($maintenance) {
			redirect('home');
		} else {
			$this->load->view('product_category',$data_content);
		}
	}
}
