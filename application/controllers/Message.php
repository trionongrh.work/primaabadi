<?php


class Message extends CI_Controller{

    public function send(){
        $post = $this->input->post();

        $output = array(
            'success' => false,
            'msg' => 'Fail'
        );

        $data = array(
            'name_inbox' => $post['values']['name'][0],
            'email_inbox' => $post['values']['email'][0],
            'phone_inbox' => $post['values']['phone'][0],
            'message_inbox' => $post['values']['message'][0],
            'at' => date('Y-m-d H:i:s'),
        );
        if($this->M_mail->addMail($data)){
            $output['success'] = true;
            $output['msg'] = 'OK';
        }
        
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($output));
    }
}