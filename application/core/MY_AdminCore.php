<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_AdminCore extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $session = $this->session->userdata('user');
        if (isset($session)) {
        } else {
            $this->session->set_flashdata('msg', 'Need Login');
            redirect('admin/login');
        }
    }

    public function createView($content, $data = array('header' => array(), 'content' => array(), 'footer' => array()))
    {
        $data['header']['unread'] = $this->M_mail->getMailSizeUnread();
        $data['header']['config'] = $this->M_settings->getSettings();
        $this->load->view('admin/templates/header', isset($data['header']) ? $data['header'] : array());
        $this->load->view($content, isset($data['content']) ? $data['content'] : array());
        $this->load->view('admin/templates/footer', isset($data['footer']) ? $data['footer'] : array());
    }

    public function notif($title = '', $message = '', $attribute = 'success',  $timeout = 3000)
    {
        $text = "setTimeout(function() {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: " . $timeout . "
            };
            toastr." . $attribute . "('" . $message . "', '" . $title . "');

        }, 1300);";

        $this->session->set_flashdata('msg', $text);
    }


    public function log_user($id_user, $status)
    {
        $this->db->where('id_user', $id_user);
        $user = $this->db->get('user')->row_array();

        $data = array(
            'id_user' => $user['id_user'],
            'username' => $user['username'],
            'status_user_log' => $status,
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'log_at' => date('Y-m-d H:i:s')
        );

        $this->db->insert('user_log', $data);
    }

    public function uploadFile($name, $path, $resize = FALSE, $props = ['height' => 509, 'width' => 800])
    {
        $this->uploadConfig($path);

        if ($this->upload->do_upload($name)) {
            $filename = $this->upload->data('file_name');
            if ($resize) {
                $this->resizeImage('./uploads/' . $path . '/' . $filename, $props);
            }
        } else {
            $filename = 'empty.jpg';
        }

        return $filename;
    }

    public function uploadConfig($path = '')
    {
        $config['upload_path']          = './uploads/' . $path;
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']             = 0;

        $this->load->library('upload', $config);
    }


    public function resizeImage($imgpath, $props = ['height' => 509, 'width' => 800])
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $imgpath;
        $config['maintain_ratio'] = FALSE;
        $config['width']         = $props['width'];
        $config['height']       = $props['height'];

        $this->load->library('image_lib', $config);

        return $this->image_lib->resize();
    }

    public function createThumb($path = '')
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = './uploads/' . $path;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']         = 320;
        $config['height']       = 240;

        $this->load->library('image_lib', $config);

        $this->image_lib->resize();
    }
}
