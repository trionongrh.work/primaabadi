<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PUMA ADMIN - <?php echo COMPNAME ?></title>
    <meta name="description" content="Multipurpose HTML template.">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/content-box.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/image-box.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/flexslider/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animations.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/components.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/php/contact-form.css">
    <link rel="stylesheet" href='<?php echo base_url() ?>assets/scripts/social.stream.css'>
    <link rel="icon" href="<?php echo base_url() ?>assets/images/favicon.png">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/skin.css">
</head>

<body>
    <div id="preloader"></div>
    <header class="fixed-top scroll-change" data-menu-anima="fade-in">
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-main navbar-middle mega-menu-fullwidth">
                <div class="container">
                    <div class="scroll-hide">
                        <div class="container">
                            <a class="navbar-brand center" href="<?php echo base_url('admin') ?>">
                                <img src="<?php echo base_url() ?>assets/images/logopumabaru.png" alt="logo" style="max-height:50px" />
                            </a>
                        </div>
                    </div>
                    <div class="navbar-header">
                        <a class="navbar-brand" href="<?php echo base_url('admin') ?>"><img src="<?php echo base_url() ?>assets/images/logopumabarusmall.png" alt="logo" /> </a>
                        <button type="button" class="navbar-toggle">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="header-base">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="title-base text-left">
                        <h1>Admin Login</h1>

                    </div>
                </div>
                <div class="col-md-3">
                    <ol class="breadcrumb b white">
                        <li><a href="#">Admin</a></li>
                        <li class="active">Login</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="section-empty section-item">
        <div class="container content">
            <div class="row">
                <div class="col-md-4">
                    <hr class="space m">
                    <form class="form-box" method="post">
                        <p>Username</p>
                        <input id="username" name="username" type="text" class="form-control form-value">
                        <hr class="space xs">
                        <p>Password</p>
                        <input id="password" name="password" type="password" class="form-control form-value">
                        <hr class="space xs">
                        <button class="btn-sm btn" type="submit">Login</button>
                        <hr class="space m">
                        <!-- <div class="success-box">
                            <div class="alert alert-success">Congratulations. Your message has been sent successfully</div>
                        </div>
                        <div class="error-box">
                            <div class="alert alert-warning">Error, please retry. Your message has not been sent</div>
                        </div> -->
                    </form>
                </div>
            </div>
        </div>
    </div>

    <i class="scroll-top scroll-top-mobile show fa fa-sort-asc"></i>
    <footer class="footer-base">
        <div class="container content">
            <div class="row">
                <div class="col-md-4">
                    <img class="logo" src="<?php echo base_url() ?>assets/images/logopumabaru.png" alt="logo" style="height: 100px" />
                    <p class="text-s">
                        PT Prima Usaha Mandiri Abadi
                    </p>
                    <hr class="space s" />
                    <div class="btn-group social-group btn-group-icons">
                        <a target="_blank" href="#" data-social="share-youtube" data-toggle="tooltip" data-placement="top" title="Youtube">
                            <i class="fa fa-youtube text-s circle"></i>
                        </a>
                        <a target="_blank" href="#" data-social="share-linkedin" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                            <i class="fa fa-linkedin text-s circle"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <h3 class="text-black text-uppercase">Contact Info</h3>
                    <hr class="space space-30" />
                    <ul class="fa-ul text-s ul-squares">
                        <li><?php echo $contact['cp_addr1'] ?></li>
                        <li><?php echo $contact['cp_addr2'] ?></li>
                        <li><?php echo $contact['cp_email'] ?></li>
                        <li><?php echo $contact['cp_phone'] ?></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3 class="text-black text-uppercase">Useful resources</h3>
                    <hr class="space space-30" />
                    <!-- Histats.com  (div with counter) -->
                    <div id="histats_counter"></div>
                    <!-- Histats.com  START  (aync)-->
                    <script type="text/javascript">
                        var _Hasync = _Hasync || [];
                        _Hasync.push(['Histats.start', '1,4367222,4,203,118,65,00001010']);
                        _Hasync.push(['Histats.fasi', '1']);
                        _Hasync.push(['Histats.track_hits', '']);
                        (function() {
                            var hs = document.createElement('script');
                            hs.type = 'text/javascript';
                            hs.async = true;
                            hs.src = ('//s10.histats.com/js15_as.js');
                            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
                        })();
                    </script>
                    <noscript><a href="/" target="_blank"><img src="//sstatic1.histats.com/0.gif?4367222&101" alt="counter free hit invisible" border="0"></a></noscript>
                    <!-- Histats.com  END  -->
                </div>
            </div>
            <hr class="space hidden-sm" />
            <div class="row copy-row">
                <div class="col-md-12 copy-text">
                    © 2020 PUMA
                </div>
            </div>
        </div>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/parallax.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/script.js"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/iconsmind/line-icons.min.css">
        <script src="<?php echo base_url() ?>assets/scripts/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/imagesloaded.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.twbsPagination.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/flexslider/jquery.flexslider-min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.tab-accordion.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/isotope.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/bootstrap/js/bootstrap.popover.min.js"></script>
        <script src='<?php echo base_url() ?>assets/scripts/php/contact-form.js'></script>
        <script src='<?php echo base_url() ?>assets/scripts/jquery.progress-counter.js'></script>
        <script src="<?php echo base_url() ?>assets/scripts/smooth.scroll.min.js"></script>
    </footer>
</body>

<!-- Mirrored from templates.framework-y.com/lightwire/pages/index-worker.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 07 Feb 2020 08:16:07 GMT -->

</html>