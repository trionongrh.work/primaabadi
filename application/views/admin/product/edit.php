<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Product</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin') ?>">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin/product') ?>">Product</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">
    <form action="<?php echo base_url('admin/product/edit_process') ?>" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $product['id_product'] ?>">
        <div class="row">
            <div class="col-lg-4">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Image Product</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="form-group row">
                            <div class="col-lg-12 text-center">
                                <img src="<?php echo $product['img_url'] ?>" alt="" width="300px" height="200px" id="img-preview">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="input-title">Image</label>
                            <div class="col-sm-10">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span><input type="file" name="imagefile" accept="image/png, image/jpeg, image/jpg" onchange="loadFile(event)" /></span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Product Details</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="input-name">Product Name</label>
                            <div class="col-sm-10">
                                <input type="text" id="input-name" class="form-control" name="name-product" placeholder="Product Name" value="<?php echo $product['nama_product'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="input-desc">Product Description</label>
                            <div class="col-sm-10">
                                <textarea name="desc-product" id="input-desc" class="summernote" rows="5" required><?php echo $product['desc_product'] ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="input-category">Category</label>
                            <div class="col-sm-10">
                                <select data-placeholder="Choose Category..." class="chosen-select" tabindex="2" id="input-category" multiple name="category[]" required>
                                    <option value=""></option>
                                    <?php
                                    $idCat = [];
                                    foreach ($product['category'] as $key) {
                                        $idCat[] = $key['id_product_category'];
                                    }
                                    foreach ($category as $key) {
                                    ?>
                                        <option value="<?php echo $key['id_product_category'] ?>" <?php echo in_array($key['id_product_category'], $idCat, true) ? 'selected' : '' ?>><?php echo $key['nama_product_category'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="text-right">
                            <a href="<?php echo base_url('admin/product') ?>" class="btn btn-white btn-sm">Back</a>
                            <button type="submit" class="btn btn-primary btn-sm" type="submit">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>