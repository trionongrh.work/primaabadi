<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Product</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin') ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Product</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Product List</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-product">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Image</th>
                                    <th>Product Name</th>
                                    <th width="30%">Category</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $num = 1;
                                foreach ($product as $key) {
                                ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td align="center"><img src="<?php echo base_url('uploads/product/'.explode('.',$key['image_product'])[0].'_thumb.'.explode('.',$key['image_product'])[1]) ?>" alt="" height="50px"></td>
                                        <td><?php echo $key['nama_product'] ?></td>
                                        <td>
                                            <?php
                                            foreach ($key['category'] as $cat) {
                                            ?>
                                            <span class="badge badge-warning"><?php echo $cat['nama_product_category'] ?></span>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('admin/product/edit?product='.$key['id_product']) ?>" class="btn btn-primary btn-xs" title="Edit Product"><i class="fa fa-pencil"></i></a>
                                            <a href="<?php echo base_url('admin/product/delete?product='.$key['id_product']) ?>" class="btn btn-danger btn-xs" title="Remove Product" onclick="return confirm('Are you sure delete this Product ?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>