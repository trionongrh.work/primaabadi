<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Visi & Misi</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin') ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Visi & Misi</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h3>Preview</h3>
                </div>
                <div class="ibox-content no-padding">
                    <iframe src="<?php echo base_url('#mn-visimisi') ?>" frameborder="0" width="100%" height="500px" id="framepreview"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <form method="POST" action="<?php echo base_url('admin/visimisi/editvisi') ?>">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Visi</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content no-padding">
                        <textarea name="form_visi" class="summernote">
                            <?php
                            echo $visi;
                            ?>
                        </textarea>

                    </div>
                    <div class="ibox-footer">
                        <div class="row">
                            <div class="col text-center">
                                <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-6">
            <form method="POST" action="<?php echo base_url('admin/visimisi/editmisi') ?>">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Misi</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content no-padding">
                        <textarea name="form_misi" class="summernote">
                            <?php
                            echo $misi;
                            ?>
                        </textarea>

                    </div>
                    <div class="ibox-footer">
                        <div class="row">
                            <div class="col text-center">
                                <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>