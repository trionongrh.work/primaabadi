<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>About</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin') ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>About</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h3>Preview</h3>
                </div>
                <div class="ibox-content no-padding">
                    <iframe src="<?php echo base_url('#mn-about') ?>" frameborder="0" width="100%" height="500px"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>About Detail</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form action="<?php echo base_url('admin/about/editdetail') ?>" enctype="multipart/form-data" method="POST">
                        <div class="form-group row">
                            <div class="col-lg-12 text-center">
                                <img src="<?php echo base_url('uploads/about/' . $aboutdetail['cp_about_image']) ?>" alt="" width="300px" height="200px" id="img-preview">
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span>
                                    <span class="fileinput-exists">Change</span><input type="file" name="imagefile" accept="image/png, image/jpeg, image/jpg" onchange="loadFile(event)" /></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" placeholder="Title" class="form-control" name="about-title" value="<?php echo $aboutdetail['cp_about_title'] ?>">
                        </div>
                        <div class="form-group">
                            <label>Description Line 1</label>
                            <textarea name="about-line1" id="about-line1" class="form-control" rows="5" placeholder="Line 1"><?php echo $aboutdetail['cp_about_line1'] ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Description Line 2</label>
                            <textarea name="about-line2" id="about-line2" class="form-control" rows="5" placeholder="Line 2"><?php echo $aboutdetail['cp_about_line2'] ?></textarea>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-lg-12 text-right">
                                <button class="btn btn-primary" type="submit">Save Changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>About List</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="<?php echo base_url('admin/about/add') ?>" class="btn btn-primary">Add About List</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-aboutlist">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $num = 1;
                                foreach ($aboutlist as $i => $key) {
                                ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $key['title_cp_about'] ?></td>
                                        <td><?php echo $key['desc_cp_about'] ?></td>
                                        <td>
                                            <a href="<?php echo base_url('admin/about/edit?about=' . $key['id_cp_about']) ?>" class="btn btn-warning btn-xs" title="Edit About List"><i class="fa fa-pencil"></i></a>
                                            <a href="<?php echo base_url('admin/about/delete?about=' . $key['id_cp_about']) ?>" class="btn btn-danger btn-xs" title="Delete About List" onclick="return confirm('Delete this About List ?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>