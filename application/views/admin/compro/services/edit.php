<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Edit Services</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin') ?>">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin/services') ?>">Services</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6 offset-lg-3">
            <div class="ibox ">
                <div class="ibox-title">
                    <a href="<?php echo base_url('admin/services') ?>" class="btn btn-success btn-xs"><i class="fa fa-arrow-left"></i> Back</a>
                    <h5>Edit Services</h5>
                </div>
                <div class="ibox-content">
                    <form action="<?php echo base_url('admin/services/edit_process') ?>" method="POST" enctype='multipart/form-data'>
                        <input type="hidden" name="id" value="<?php echo $services['id_cp_services'] ?>">
                        <div class="form-group row">
                            <div class="col-lg-12 text-center">
                                <img src="<?php echo $services['img_url'] ?>" alt="" width="300px" height="200px" id="img-preview">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="input-title">Image</label>
                            <div class="col-sm-10">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span><input type="file" name="imagefile" accept="image/png, image/jpeg, image/jpg" onchange="loadFile(event)"/></span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                </div>
                                <span class="form-text m-b-none"><small><i>Select Image file if only want to change</i></small></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="input-title">Title</label>
                            <div class="col-sm-10">
                                <input type="text" id="input-title" class="form-control" name="title" placeholder="Title" required value="<?php echo $services['title_cp_services'] ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="input-icon">Icon</label>
                            <div class="col-sm-10">
                                <input type="text" id="input-icon" class="form-control" name="icon" placeholder="Icon ( im-* )" required value="<?php echo $services['icon_cp_services'] ?>">
                                <span class="form-text m-b-none"><small><i>See icomoon pack <a href="https://icomoon.io/#preview-ultimate" target="_blank">here</a>. please use <code>im-</code> before icon name ex. <code>im-pencil</code></i></small></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="input-title">Icon</label>
                            <div class="col-sm-10">
                                <textarea name="description" id="input-description" placeholder="Description" rows="5" class="form-control" required><?php echo $services['desc_cp_services'] ?></textarea>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-lg-12 text-right">
                                <button class="btn btn-primary btn-sm" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>