<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Services</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin') ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Services</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h3>Preview</h3>
                </div>
                <div class="ibox-content no-padding">
                    <iframe src="<?php echo base_url('#mn-services') ?>" frameborder="0" width="100%" height="500px"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Services</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="<?php echo base_url('admin/services/add') ?>" class="btn btn-primary">Add Services</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-services">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Title</th>
                                    <th>Icon</th>
                                    <th>Description</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $num = 1;
                                foreach ($services as $i => $key) {
                                ?>
                                    <tr>
                                        <td><?php echo $num++ ?></td>
                                        <td><?php echo $key['title_cp_services'] ?></td>
                                        <td><i><code><?php echo $key['icon_cp_services'] ?></code></i></td>
                                        <td><?php echo $key['desc_cp_services'] ?></td>
                                        <td>
                                            <a href="<?php echo base_url('admin/services/edit?services=' . $key['id_cp_services']) ?>" class="btn btn-warning btn-xs" title="Edit Services"><i class="fa fa-pencil"></i></a>
                                            <a href="<?php echo base_url('admin/services/delete?services=' . $key['id_cp_services']) ?>" class="btn btn-danger btn-xs" title="Delete Services" onclick="return confirm('Delete this Service ?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>