<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Slider</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin') ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Slider</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h3>Preview</h3>
                </div>
                <div class="ibox-content no-padding">
                    <iframe src="<?php echo base_url() ?>" frameborder="0" width="100%" height="500px" id="framepreview"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Slider</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">
                                Add Slide
                            </button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-slider">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Thumbnail</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($slider as $sld) {
                                ?>
                                    <tr>
                                        <td><?php echo $no++ ?></td>
                                        <td align="center"><img src="<?php echo $sld['img_url'] ?>" height="100px"></td>
                                        <td align="center">
                                            <a href="<?php echo base_url('admin/slider_home/delete/'.$sld['id_cp_slider']) ?>" class="btn btn-danger btn-xs" title="Remove Slider" onclick="return confirm('Are you sure delete this Slide ?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox ">
                <form action="<?php echo base_url('admin/slider_home/update_config') ?>" method="POST">
                    <div class="ibox-title">
                        <h5>Slider Config</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label" for="input-title">Control Nav <small>(Bottom)</small></label>
                            <div class="col-sm-8">
                                <input type="checkbox" class="js-switch" name="sliderControlNav" <?php echo $config['sliderControlNav'] ? 'checked' : '' ?> />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label" for="input-title">Control Nav <small>(Arrow)</small></label>
                            <div class="col-sm-8">
                                <input type="checkbox" class="js-switch2" name="sliderControlArrow" <?php echo $config['sliderControlArrow'] ? 'checked' : '' ?> />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label" for="input-title">Slider Speed</label>
                            <div class="col-sm-8">
                                <div class="input-group m-b">
                                    <input type="number" class="form-control" name="sliderSpeed" value="<?php echo $config['sliderSpeed'] != "" ? $config['sliderSpeed'] : 0 ?>" />
                                    <div class="input-group-append">
                                        <span class="input-group-addon">ms</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-footer">
                        <div class="row">
                            <div class="col text-center">
                                <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="addModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeInDown">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Add Slide</h4>
            </div>
            <form action="<?php echo base_url('admin/slider_home/add_process') ?>" enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12 text-center">
                            <img src="<?php echo base_url() ?>assets/images/empty.jpg" alt="" width="300px" height="200px" id="img-preview">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="input-title">Image</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span>
                                    <span class="fileinput-exists">Change</span><input type="file" name="imagefile" accept="image/png, image/jpeg, image/jpg" onchange="loadFile(event)" required /></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                        </div>
                        <small><i>Minimum image size <code>1280x720</code> for better quality</i></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>