<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Contact</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin') ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Contact</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Contact</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form action="<?php echo base_url('admin/contact/updatecontact') ?>" method="POST">
                        <div class="form-group">
                            <label>Address 1</label>
                            <textarea name="cp_addr1" id="cp_addr1" rows="3" class="form-control" placeholder="Enter Address"><?php echo $contact['cp_addr1'] ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Address 2</label>
                            <input type="text" name="cp_addr2" placeholder="Enter address" class="form-control" value="<?php echo $contact['cp_addr2'] ?>">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="cp_email" placeholder="Enter email" class="form-control" value="<?php echo $contact['cp_email'] ?>">
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="cp_phone" placeholder="Enter phone number" class="form-control" value="<?php echo $contact['cp_phone'] ?>">
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <h2>
                    Mailbox <?php echo $mail_size > 0 ? '(' . $mail_size . ')' : '' ?>
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Read all mails"><i class="fa fa-eye"></i> </button>
                    <!-- <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </button> -->

                </div>
            </div>
            <div class="mail-box">

                <table class="table table-hover table-mail">
                    <tbody>
                        <?php
                        foreach ($mail as $key) {
                        ?>
                            <tr class="<?php echo $key['is_read'] ? 'read' : 'unread' ?>">
                                <td class="mail-ontact" style="vertical-align:middle"><a href="<?php echo base_url('admin/contact/mail?mail='.$key['id_inbox']) ?>"><?php echo $key['name_inbox'] ?></a></td>
                                <td class="mail-subject" style="vertical-align:middle" width="50%"><a href="<?php echo base_url('admin/contact/mail?mail='.$key['id_inbox']) ?>"><?php echo strlen($key['message_inbox']) < 100 ? $key['message_inbox'] : substr($key['message_inbox'], 0, 100) . '...' ?></a></td>
                                <?php
                                    $at = date_create($key['at']);
                                    $now = date_create(date('Y-m-d H:i:s'));

                                    $interval = date_diff($at, $now);
                                    $format = '';
                                    if($interval->h > 0){
                                        $format = '%h hours ago';
                                    }else
                                    if($interval->m > 0){
                                        $format = '%i minutes ago';
                                    }else{
                                        $format = '%s seconds ago';
                                    }
                                ?>
                                <td class="text-right mail-date" style="vertical-align:middle"><?php echo $interval->d > 0 ? date_format($at, 'H:iA').'<br>'.date_format($at, 'd F Y') : $interval->format($format) ?></td>
                                <td class="text-center" style="vertical-align:middle">
                                    <a href="" class="btn btn-white btn-sm" title="Mark as read"><i class="fa fa-eye"></i></a>
                                    <a href="<?php echo base_url('admin/contact/maildelete?mail='.$key['id_inbox']) ?>" class="btn btn-white btn-sm" title="Delete mail"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</div>