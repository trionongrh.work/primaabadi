<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Mailbox</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin') ?>">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin/contact') ?>">Contact</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Mailbox</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Contact</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form action="<?php echo base_url('admin/contact/updatecontact') ?>" method="POST">
                        <div class="form-group">
                            <label>Address 1</label>
                            <textarea name="cp_addr1" id="cp_addr1" rows="3" class="form-control" placeholder="Enter Address"><?php echo $contact['cp_addr1'] ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Address 2</label>
                            <input type="text" name="cp_addr2" placeholder="Enter address" class="form-control" value="<?php echo $contact['cp_addr2'] ?>">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="cp_email" placeholder="Enter email" class="form-control" value="<?php echo $contact['cp_email'] ?>">
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="cp_phone" placeholder="Enter phone number" class="form-control" value="<?php echo $contact['cp_phone'] ?>">
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <a href="<?php echo base_url('admin/contact') ?>" class="btn btn-success btn-xs float-right"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <h2>
                            View Message
                        </h2>
                    </div>
                    <div class="col-lg-6">
                        <div class="float-right tooltip-demo">
                            <a href="<?php echo base_url('admin/contact/maildelete?mail=' . $mail['id_inbox']) ?>" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Delete mail"><i class="fa fa-trash-o"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="mail-tools tooltip-demo m-t-md">
                    <div class="row">
                        <div class="col">
                            <h4>From : </h4>
                            <h5><?php echo $mail['name_inbox'] ?></h5>
                            <h5><a href="mailto:<?php echo $mail['email_inbox'] ?>"><?php echo $mail['email_inbox'] ?></a></h5>
                            <h5><?php echo $mail['phone_inbox'] ?></h5>
                        </div>
                        <div class="col">
                            <span class="float-right font-normal"><?php echo date_format(date_create($mail['at']), 'd F Y, H:iA ') ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mail-box">
                <div class="mail-body">
                    <?php echo $mail['message_inbox'] ?>
                </div>
            </div>
        </div>
    </div>
</div>