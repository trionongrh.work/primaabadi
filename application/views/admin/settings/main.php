<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>General Settings</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url('admin') ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>General Settings</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6 offset-lg-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>General Settings</h5>
                </div>
                <form action="<?php echo base_url('admin/settings/update_config') ?>" enctype="multipart/form-data" method="POST">
                    <div class="ibox-content">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="input-title">Maintenance Mode</label>
                            <div class="col-sm-9">
                                <input type="checkbox" class="js-switch" name="maintenanceMode" <?php echo $config['maintenanceMode'] ? 'checked' : '' ?> />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="input-title">Link 1 ( Youtube )</label>
                            <div class="col-sm-9">
                                <div class="input-group m-b">
                                    <div class="input-group-prepend">
                                        <span class="input-group-addon"><i class="fa fa-youtube"></i></span>
                                    </div>
                                    <input type="text" name="link1" id="cp-link-1" class="form-control" placeholder="https://www.youtube.com//watch?v=..." value="<?php echo $config['link1'] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="input-title">Link 2 ( LinkedIn )</label>
                            <div class="col-sm-9">
                                <div class="input-group m-b">
                                    <div class="input-group-prepend">
                                        <span class="input-group-addon"><i class="fa fa-linkedin-square"></i></span>
                                    </div>
                                    <input type="text" name="link2" id="cp-link-2" class="form-control" placeholder="https://www.linkedin.com/in/..." value="<?php echo $config['link2'] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3 ">
                                <label for="">Background <br/>(Visi & Misi)</label>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span><input type="file" name="imagevisi" accept="image/png, image/jpeg, image/jpg" onchange="loadFile(event,'img-visi-misi')" /></span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                </div>
                            </div>
                            <img src="<?php echo $config['home_visimisi_img'] ?>" alt="" class="col-sm-9" id="img-visi-misi" style="max-height:200px;">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3 ">
                                <label for="">Background <br/>(Contact Us)</label>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span><input type="file" name="imagecontact" accept="image/png, image/jpeg, image/jpg" onchange="loadFile(event,'img-contact-us')" /></span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                </div>
                            </div>
                            <img src="<?php echo $config['home_contact_img'] ?>" alt="" class="col-sm-9" id="img-contact-us" style="max-height:200px;">
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>