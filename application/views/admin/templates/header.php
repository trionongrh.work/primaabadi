<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.9.2/layouts.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Nov 2019 10:00:22 GMT -->

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PUMA ADMIN - <?php echo COMPNAME ?></title>

    <link href="<?php echo base_url() ?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo base_url() ?>assets/admin/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/admin/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/admin/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/admin/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/admin/css/plugins/summernote/summernote-bs4.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/admin/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/admin/css/plugins/iCheck/custom.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
    
    <link href="<?php echo base_url() ?>assets/admin/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link rel="icon" href="<?php echo base_url() ?>assets/images/logopumabarulight.png">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <?php
                    $if_header_exist = isset($page);
                    $page = $if_header_exist ? $page : '';
                    $if_header_sub_exist = isset($sub_page);
                    $sub_page = $if_header_sub_exist ? $sub_page : '';
                    function checkPage($page, $active_mode)
                    {
                        $active_class = 'active';
                        switch ($page) {
                            case $active_mode:
                                return $active_class;
                            default:
                                return '';
                        }
                    }
                    ?>
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <img alt="image" class="rounded-circle" src="<?php echo base_url() ?>assets/admin/img/profile_small.jpg" />
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="block m-t-xs font-bold"><?php echo $this->session->userdata('user')['nama_user'] ?></span>
                                <span class="text-muted text-xs block"><?php echo $this->session->userdata('user')['name_level'] ?> <b class="caret"></b></span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="dropdown-item" href="profile.html">Profile</a></li>
                                <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>
                                <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>
                                <li class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="<?php echo base_url('admin/logout') ?>">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            PUMA
                        </div>
                    </li>
                    <li class="<?php echo checkPage($page, 'admin_home') ?>">
                        <a href="<?php echo base_url('admin') ?>"><i class="fa fa-home"></i> <span class="nav-label">Home</span>
                        </a>
                    </li>
                    <li class="<?php echo checkPage($page, 'admin_company') ?>">
                        <a href="index-2.html"><i class="fa fa-th-large"></i> <span class="nav-label">Company Profile</span>
                            <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="<?php echo checkPage($sub_page, 'company_slider') ?>"><a href="<?php echo base_url('admin/slider_home') ?>">Slider</a></li>
                            <li class="<?php echo checkPage($sub_page, 'company_aboutus') ?>"><a href="<?php echo base_url('admin/about') ?>">About Us</a></li>
                            <li class="<?php echo checkPage($sub_page, 'company_visimisi') ?>"><a href="<?php echo base_url('admin/visimisi') ?>">Visi & Misi</a></li>
                            <li class="<?php echo checkPage($sub_page, 'company_services') ?>"><a href="<?php echo base_url('admin/services') ?>">Services</a></li>
                            <li class="<?php echo checkPage($sub_page, 'company_contact') ?>"><a href="<?php echo base_url('admin/contact') ?>">Contact & Mailbox<span class="label <?php echo $unread > 0 ? 'label-primary' : 'label-success' ?> float-right"><?php echo $unread < 99 ? $unread : '99+' ?></span></a></li>
                        </ul>
                    </li>
                    <li class="<?php echo checkPage($page, 'admin_product') ?>">
                        <a href="index-2.html"><i class="fa fa-car"></i> <span class="nav-label">Product</span>
                            <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="<?php echo checkPage($sub_page, 'product_add') ?>"><a href="<?php echo base_url('admin/product/add') ?>">Add Product</a></li>
                            <li class="<?php echo checkPage($sub_page, 'product_main') ?>"><a href="<?php echo base_url('admin/product') ?>">Product List</a></li>
                            <li class="<?php echo checkPage($sub_page, 'product_category') ?>"><a href="<?php echo base_url('admin/product_category') ?>">Product Category</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo checkPage($page, 'admin_settings') ?>">
                        <a href="index-2.html"><i class="fa fa-gear"></i> <span class="nav-label">Settings</span>
                            <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="<?php echo checkPage($sub_page, 'settings_general') ?>"><a href="<?php echo base_url('admin/settings') ?>">General Settings</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="<?php echo base_url('admin/settings') ?>"><span class="m-r-sm text-muted welcome-message">Maintenance Mode : <span class="badge <?php echo $config['maintenanceMode'] ? 'badge-warning' : 'badge-gray' ?>"><?php echo $config['maintenanceMode'] ? 'ON' : 'OFF' ?></span></span></a>
                        </li>
                        <li>
                            <div>
                                <a href="<?php echo base_url() ?>" class="btn btn-primary text-white">
                                    <i class="fa fa-sign-out"></i> Website
                                </a>
                            </div>
                        </li>

                        <li>
                            <a href="<?php echo base_url('admin/logout') ?>">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>

                </nav>
            </div>