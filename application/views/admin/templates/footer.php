<div class="footer">
    <div>
        &copy; 2020 <strong>PUMA</strong>
    </div>
</div>
</div>

</div>
<?php

$if_header_exist = isset($page);
$page = $if_header_exist ? $page : '';
$if_header_sub_exist = isset($sub_page);
$sub_page = $if_header_sub_exist ? $sub_page : '';
function checkFoot($page, $active_mode)
{
    return $page == $active_mode;
}
?>
<!-- Mainly scripts -->
<script src="<?php echo base_url() ?>assets/admin/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/popper.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script src="<?php echo base_url() ?>assets/admin/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

<!-- Flot -->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/flot/jquery.flot.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/peity/jquery.peity.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url() ?>assets/admin/js/inspinia.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<!-- <script src="<?php echo base_url() ?>assets/admin/js/plugins/jquery-ui/jquery-ui.min.js"></script> -->

<!-- GITTER -->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="<?php echo base_url() ?>assets/admin/js/demo/sparkline-demo.js"></script>

<!-- ChartJS-->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/chartJs/Chart.min.js"></script>

<!-- Toastr -->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/toastr/toastr.min.js"></script>

<!-- Chosen -->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/chosen/chosen.jquery.js"></script>

<!-- SUMMERNOTE -->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/summernote/summernote-bs4.js"></script>

<!-- iCheck -->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/iCheck/icheck.min.js"></script>

<!-- Switchery -->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/switchery/switchery.js"></script>

<!-- Jasny -->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<script>
    var base_url = "<?php echo base_url() ?>";
    var site_url = "<?php echo site_url() ?>";
    $(document).ready(function() {
        <?php echo $this->session->flashdata('msg') ?>
    });
</script>
<?php
if (checkFoot($sub_page, 'product_main')) {
?>
    <script>
        $(document).ready(function() {
            $('.dataTables-product').DataTable({
                pageLength: 10,
                responsive: true,
                dom: 'Tfgtp',

            });
        })
    </script>
<?php
}
?>
<?php
if (checkFoot($sub_page, 'company_visimisi')) {
?>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 200, //set editable area's height
                toolbar: [
                    ['style', ['style']],
                    ['style', ['bold', 'italic', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['code', ['codeview']],
                ]
            });
        })
    </script>
<?php
}
?>
<?php
if (checkFoot($sub_page, 'company_services')) {
?>
    <script>
        $(document).ready(function() {
            $('.dataTables-services').DataTable({
                pageLength: 25,
                responsive: true,
                dom: 'Tfgtip',

            });


        })
        var loadFile = function(event) {
            var output = document.getElementById('img-preview');
            if (event.target.files[0] != null) {
                output.src = URL.createObjectURL(event.target.files[0]);
            } else {
                output.src = '<?php echo base_url('assets/images/empty.jpg') ?>';
            }
        };
    </script>
<?php
}
?>
<?php
if (checkFoot($sub_page, 'company_contact')) {
?>
    <script>
        $(document).ready(function() {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        })
    </script>
<?php
}
?>
<?php
if (checkFoot($sub_page, 'settings_general')) {
?>
    <script>
        $(document).ready(function() {
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, {
                color: '#1AB394'
            });
        })
        
        var loadFile = function(event,id) {
            var output = document.getElementById(id);
            if (event.target.files[0] != null) {
                output.src = URL.createObjectURL(event.target.files[0]);
            } else {
                output.src = '<?php echo base_url('assets/images/empty.jpg') ?>';
            }
        };
    </script>
<?php
}
?>
<?php
if (checkFoot($sub_page, 'company_aboutus')) {
?>
    <script>
        $(document).ready(function() {
            $('.dataTables-aboutlist').DataTable({
                pageLength: 25,
                responsive: true,
                dom: 'Tfgtip',

            });


        })
        var loadFile = function(event) {
            var output = document.getElementById('img-preview');
            if (event.target.files[0] != null) {
                output.src = URL.createObjectURL(event.target.files[0]);
            } else {
                output.src = '<?php echo base_url('assets/images/empty.jpg') ?>';
            }
        };
    </script>
<?php
}
?>
<?php
if (checkFoot($sub_page, 'company_slider')) {
?>
    <script>
        $(document).ready(function() {
            $('.dataTables-slider').DataTable({
                pageLength: 25,
                responsive: true,
                dom: 'Tfgtip',

            });
            var elem = [
                document.querySelector('.js-switch'),
                document.querySelector('.js-switch2')
            ];
            var switchery = [
                new Switchery(elem[0], {
                    color: '#1AB394'
                }),
                new Switchery(elem[1], {
                    color: '#1AB394'
                })
            ];

        })

        var loadFile = function(event) {
            var output = document.getElementById('img-preview');
            if (event.target.files[0] != null) {
                output.src = URL.createObjectURL(event.target.files[0]);
            } else {
                output.src = '<?php echo base_url('assets/images/empty.jpg') ?>';
            }
        };
    </script>
<?php
}
?>
<?php
if (checkFoot($sub_page, 'product_category')) {
?>
    <script>
        $formedit = $('#form_category_edit');
        $modalEdit = $('#editModal');
        $(document).ready(function() {
            $('.dataTables-category').DataTable({
                pageLength: 25,
                responsive: true,
                dom: 'Tfgtip',

            });

            $('.btn_category_edit').click(function(e) {
                var id = $(this).data('id');

                $.ajax({
                    url: base_url + 'admin/product_category/ajax_get_category',
                    data: {
                        category: id
                    },
                    success: function(response) {
                        if (response.status) {
                            $('#category_name_edit').val(response.data.nama_product_category);
                            $('#id_category_edit').val(id);
                            $modalEdit.modal('show');
                        }
                    }
                })
            })
        })
    </script>
<?php
}
?>
<?php
if (checkFoot($sub_page, 'product_add')) {
?>
    <script>
        $(document).ready(function() {
            // $('.dataTables-aboutlist').DataTable({
            //     pageLength: 25,
            //     responsive: true,
            //     dom: 'Tfgtip',

            // });


            $('.summernote').summernote({
                height: 200,
                toolbar: [
                    ['style', ['style']],
                    ['style', ['bold', 'italic', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['code', ['codeview']],
                ]
            });
            $('.chosen-select').chosen({
                max_selected_options: 3,
                width: "100%"
            });
            $(".chosen-select").bind("chosen:maxselected", function() {
                alert('Category maximum is 3');
            });
        })
        var loadFile = function(event) {
            var output = document.getElementById('img-preview');
            if (event.target.files[0] != null) {
                output.src = URL.createObjectURL(event.target.files[0]);
            } else {
                output.src = '<?php echo base_url('assets/images/empty.jpg') ?>';
            }
        };
    </script>
<?php
}
?>
<?php
if (checkFoot($sub_page, 'product_edit')) {
?>
    <script>
        $(document).ready(function() {
            // $('.dataTables-aboutlist').DataTable({
            //     pageLength: 25,
            //     responsive: true,
            //     dom: 'Tfgtip',

            // });


            $('.summernote').summernote({
                height: 200,
                toolbar: [
                    ['style', ['style']],
                    ['style', ['bold', 'italic', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['code', ['codeview']],
                ]
            });
            $('.chosen-select').chosen({
                max_selected_options: 3,
                width: "100%"
            });
            $(".chosen-select").bind("chosen:maxselected", function() {
                alert('Category maximum is 3');
            });
        })
        var loadFile = function(event) {
            var output = document.getElementById('img-preview');
            if (event.target.files[0] != null) {
                output.src = URL.createObjectURL(event.target.files[0]);
            } else {
                output.src = '<?php echo base_url('assets/images/empty.jpg') ?>';
            }
        };
    </script>
<?php
}
?>
</body>


</html>