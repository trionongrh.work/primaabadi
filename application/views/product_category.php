<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo COMPNAME ?></title>
    <meta name="description" content="Manufaktur Karoseri terintegrasi.">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/content-box.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/image-box.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/flexslider/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animations.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/components.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/php/contact-form.css">
    <link rel="stylesheet" href='<?php echo base_url() ?>assets/scripts/social.stream.css'>
    <link rel="icon" href="<?php echo base_url() ?>assets/images/logopumabarulight.png">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/skin.css">
</head>

<body>
    <div id="preloader"></div>

    <header class="fixed-top scroll-change" data-menu-anima="fade-bottom">
        <div class="navbar navbar-default icon-menu mega-menu-fullwidth navbar-fixed-top" role="navigation" id="header-nav">
            <div class="navbar navbar-main">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url() ?>" style="padding-right:0">
                            <div class="scroll-hide">
                                <table>
                                    <tr>
                                        <td><img src="<?php echo base_url() ?>assets/images/logopumabarusmall.png" alt="logo" /></td>
                                        <td class="text-right">
                                            <h3 class="text-center"><?php echo COMPNAME ?></h3>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <img class="scroll-show" src="<?php echo base_url() ?>assets/images/logopumabaru.png" alt="logo" style="max-height: 40px" />
                        </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <div class="nav navbar-nav navbar-right">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="<?php echo base_url() ?>" role="button">Home <span class="caret"></span></a>
                                </li>
                                <li class="dropdown">
                                    <a href="<?php echo base_url('#mn-about') ?>" role="button">About Us <span class="caret"></span></a>
                                </li>
                                <li class="dropdown">
                                    <a href="<?php echo base_url('#mn-services') ?>" role="button">Services <span class="caret"></span></a>
                                </li>
                                <li class="dropdown">
                                    <a href="<?php echo base_url('product/category') ?>" class="dropdown-toggle" data-toggle="dropdown" role="button">Products <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right multi-level">
                                        <?php
                                        foreach ($product_cat as $key) {
                                        ?>
                                            <li><a href="<?php echo base_url('product/category/' . $key['key_filter']) ?>"><?php echo $key['nama_product_category'] ?></a></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="<?php echo base_url('#mn-contact') ?>" role="button">Contact Us <span class="caret"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="section-empty">
        <div class="container-fluid content">
            <div class="tab-box left" data-tab-anima="fade-in">
                <ul class="nav nav-tabs col-md-2">
                    <?php
                    foreach ($product_cat as $i => $key) {
                    ?>
                        <li class="<?php echo $activekey != '' ? ($key['key_filter'] == $activekey ? 'active' : '') : ($i == 0 ? 'active' : ''); ?>"><a><?php echo $key['nama_product_category'] ?></a></li>
                    <?php
                    }
                    ?>
                </ul>
                <div class="panel-box col-md-10">
                    <?php
                    foreach ($product_cat as $i => $key) {
                    ?>
                        <div class="panel <?php echo $activekey != '' ? ($key['key_filter'] == $activekey ? 'active' : '') : ($i == 0 ? 'active' : ''); ?>">
                            <h2><?php echo $key['nama_product_category'] ?></h2>
                            <?php
                            if (sizeof($key['product']) == 0) {
                            ?>
                                <p class="text-muted">No Product...</p>
                            <?php
                            }
                            ?>
                            <hr class="space l" />
                            <div class="grid-list">
                                <div class="grid-box row">
                                    <?php
                                    foreach ($key['product'] as $prod) {
                                    ?>
                                        <div class="grid-item col-md-3">
                                            <div class="img-box adv-img adv-img-classic-box">
                                                <a class="img-box" href="<?php echo base_url('product/detail/'.$prod['id_product']) ?>">
                                                    <span><img alt="" src="<?php echo $prod['img_url'] ?>"></span>
                                                </a>
                                                <!-- <div class="caption">
                                                    <div class="caption-inner">
                                                        <h2><?php echo $prod['nama_product'] ?></h2>
                                                        <p class="sub-text"> <?php echo $key['nama_product_category'] ?> </p>
                                                        <p class="big-text">
                                                            <?php echo strip_tags($prod['desc_product']) ?>
                                                        </p>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="clear"></div>
                                <ul class="pagination-sm pagination-grid hide-first-last" data-current-page="1" data-page-items="<?php echo $itemPerpage ?>" data-pagination-anima="fade-left"></ul>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="section-empty bg-white">
        <div class="container content">
            <table width="100%">
                <tbody>
                    <tr>
                        <td class="text-center"><img src="<?php echo base_url() ?>assets/images/vendor1.jpg" alt="" style="height:200px"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <i class="scroll-top scroll-top-mobile show fa fa-sort-asc" id="scrolltotop"></i>
    <footer class="footer-base">
        <div class="container content">
            <div class="row">
                <div class="col-md-4">
                    <img class="logo" src="<?php echo base_url() ?>assets/images/logopumabaru.png" alt="logo" style="height: 100px" />
                    <p class="text-s">
                        PT Prima Usaha Mandiri Abadi
                    </p>
                    <hr class="space s" />
                    <div class="btn-group social-group btn-group-icons">
                        <a target="_blank" href="<?php echo $setting['link1'] ?>" data-toggle="tooltip" data-placement="top" title="Youtube">
                            <i class="fa fa-youtube text-s circle"></i>
                        </a>
                        <a target="_blank" href="<?php echo $setting['link2'] ?>" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                            <i class="fa fa-linkedin text-s circle"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <h3 class="text-black text-uppercase">Contact Info</h3>
                    <hr class="space space-30" />
                    <ul class="fa-ul text-s ul-squares">
                        <li><?php echo $contact['cp_addr1'] ?></li>
                        <li><?php echo $contact['cp_addr2'] ?></li>
                        <li><?php echo $contact['cp_email'] ?></li>
                        <li><?php echo $contact['cp_phone'] ?></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3 class="text-black text-uppercase">Useful resources</h3>
                    <hr class="space space-30" />
                    <!-- Histats.com  (div with counter) -->
                    <div id="histats_counter"></div>
                    <!-- Histats.com  START  (aync)-->
                    <script type="text/javascript">
                        var _Hasync = _Hasync || [];
                        _Hasync.push(['Histats.start', '1,4367222,4,203,118,65,00001010']);
                        _Hasync.push(['Histats.fasi', '1']);
                        _Hasync.push(['Histats.track_hits', '']);
                        (function() {
                            var hs = document.createElement('script');
                            hs.type = 'text/javascript';
                            hs.async = true;
                            hs.src = ('//s10.histats.com/js15_as.js');
                            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
                        })();
                    </script>
                    <noscript><a href="/" target="_blank"><img src="//sstatic1.histats.com/0.gif?4367222&101" alt="counter free hit invisible" border="0"></a></noscript>
                    <!-- Histats.com  END  -->
                </div>
            </div>
            <hr class="space hidden-sm" />
            <div class="row copy-row">
                <div class="col-md-12 copy-text">
                    © 2020 PUMA
                </div>
            </div>
        </div>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/script.js"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/iconsmind/line-icons.min.css">
        <script src="<?php echo base_url() ?>assets/scripts/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/imagesloaded.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.twbsPagination.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/flexslider/jquery.flexslider-min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.tab-accordion.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/isotope.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/bootstrap/js/bootstrap.popover.min.js"></script>
        <script src='<?php echo base_url() ?>assets/scripts/php/contact-form.js'></script>
        <script src='<?php echo base_url() ?>assets/scripts/jquery.progress-counter.js'></script>
        <script src="<?php echo base_url() ?>assets/scripts/smooth.scroll.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/parallax.min.js"></script>
        <script>
            $(document).ready(function(){
                $('.viewbox').click(function(e){
                    return false;
                })
            })
        </script>
    </footer>
</body>

<!-- Mirrored from templates.framework-y.com/lightwire/pages/index-worker.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 07 Feb 2020 08:16:07 GMT -->

</html>