<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo COMPNAME ?></title>
    <meta name="description" content="Manufaktur Karoseri terintegrasi.">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/content-box.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/image-box.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/flexslider/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animations.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/components.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/php/contact-form.css">
    <link rel="stylesheet" href='<?php echo base_url() ?>assets/scripts/social.stream.css'>
    <link rel="icon" href="<?php echo base_url() ?>assets/images/logopumabarulight.png">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/skin.css">
</head>

<body>
    <div id="preloader"></div>

    <header class="fixed-top scroll-change" data-menu-anima="fade-bottom">
        <div class="navbar navbar-default icon-menu mega-menu-fullwidth navbar-fixed-top" role="navigation" id="header-nav">
            <div class="navbar navbar-main">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url() ?>" style="padding-right:0">
                            <div class="scroll-hide">
                                <table>
                                    <tr>
                                        <td><img src="<?php echo base_url() ?>assets/images/logopumabarusmall.png" alt="logo" /></td>
                                        <td class="text-right">
                                            <h3 class="text-center"><?php echo COMPNAME ?></h3>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <img class="scroll-show" src="<?php echo base_url() ?>assets/images/logopumabaru.png" alt="logo" style="max-height: 40px" />
                        </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <div class="nav navbar-nav navbar-right">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="<?php echo base_url() ?>" role="button">Home <span class="caret"></span></a>
                                </li>
                                <li class="dropdown">
                                    <a href="<?php echo base_url('#mn-about') ?>" role="button">About Us <span class="caret"></span></a>
                                </li>
                                <li class="dropdown">
                                    <a href="<?php echo base_url('#mn-services') ?>" role="button">Services <span class="caret"></span></a>
                                </li>
                                <li class="dropdown">
                                    <a href="<?php echo base_url('product/category') ?>" class="dropdown-toggle" data-toggle="dropdown" role="button">Products <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right multi-level">
                                        <?php
                                        foreach ($product_cat as $key) {
                                        ?>
                                            <li><a href="<?php echo base_url('product/category/' . $key['key_filter']) ?>"><?php echo $key['nama_product_category'] ?></a></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="<?php echo base_url('#mn-contact') ?>" role="button">Contact Us <span class="caret"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="section-slider">
        <div class="flexslider slider white" data-options="animation:fade,controlNav:<?php echo $slider_config['sliderControlNav'] ? 'true' : 'false' ?>,slideshowSpeed:<?php echo $slider_config['sliderSpeed'] ?>,directionNav:<?php echo $slider_config['sliderControlArrow'] ? 'true' : 'false' ?>">
            <ul class="slides">
                <?php
                foreach ($slider as $key) {
                ?>
                    <li>
                        <div class="bg-cover" style="background-image:url('<?php echo $key['img_url'] ?>')"></div>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
        <div class="container content overlay-content white">
            <div class="row">
                <div class="col-md-6" data-anima="fade-left">
                    <hr class="space l" />
                    <hr class="space l" />
                    <hr class="space l" />
                    <hr class="space l" />
                    <!-- <h1 class="text-uppercase">We are focused to<br /> “QUALITY SATISFACTION”</h1> -->
                    <hr class="space l" />
                </div>
            </div>
            <hr class="space space-250" />
        </div>
    </div>

    <div class="section-empty bg-white" id="mn-about">
        <div class="container content">
            <div class="row">
            </div>
            <div class="row vertical-row">
                <div class="col-md-6">
                    <a class="img-box thumbnail" href="#">
                        <img src="<?php echo base_url('uploads/about/' . $about['cp_about_image']) ?>" alt="">
                    </a>
                </div>
                <div class="col-md-6">
                    <hr class="space visible-sm">
                    <hr class="space visible-sm">
                    <h2><?php echo $about['cp_about_title'] ?></h2>
                    <p align="justify"><?php echo $about['cp_about_line1'] ?></p>
                    <p align="justify"><?php echo $about['cp_about_line2'] ?></p>
                    <hr class="space m">
                </div>
            </div>
        </div>
    </div>

    <div class="section-empty">
        <div class="container content">
            <div class="row">
                <div class="col-md-12">
                    <div class="flexslider carousel outer-navs" data-options="minWidth:225,itemMargin:60,numItems:3,controlNav:true,directionNav:true,slideshowSpeed:3000">
                        <ul class="slides">
                            <?php
                            foreach ($aboutlist as $i => $key) {
                            ?>
                                <li>
                                    <div class="advs-box advs-box-top-icon-img boxed-inverse text-left">
                                        <a class="img-box lightbox img-scale-up" href="#">
                                            <span><img src="<?php echo $key['img_url'] ?>" alt=""></span>
                                        </a>
                                        <div class="advs-box-content">
                                            <h3><?php echo $key['title_cp_about'] ?></h3>
                                            <p style="text-align:justify">
                                                <?php echo $key['desc_cp_about'] ?>
                                            </p>
                                            <hr class="space m" />
                                            <hr class="space m" />
                                        </div>
                                    </div>
                                </li>
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-bg-image parallax-window white" data-natural-height="800" data-natural-width="1920" data-parallax="scroll" data-image-src="<?php echo $setting['home_visimisi_img'] ?>" id="mn-visimisi">
        <div class="container content">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <h2 class="text-uppercase">VISI</h2>
                    <hr class="space l" />
                    <?php
                    echo $visi;
                    ?>
                </div>
                <div class="col-md-6 col-sm-12">
                    <h2 class="text-uppercase">MISI</h2>
                    <hr class="space l" />
                    <?php
                    echo $misi;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty" id="mn-services">
        <div class="container content">
            <table class="grid-table grid-table-xs-12 text-left">
                <tbody>
                    <tr>
                        <td>
                            <h2>Our Services</h2>
                        </td>
                        <td>

                        </td>
                    </tr>
                </tbody>
            </table>
            <hr class="space" />
            <div class="flexslider carousel outer-navs" data-options="minHeight:100,minWidth:200,itemMargin:30,numItems:3,controlNav:true,directionNav:true">
                <ul class="slides">
                    <?php
                    foreach ($services as $i => $key) {
                    ?>
                        <li>
                            <div class="advs-box advs-box-multiple boxed-inverse" data-anima="scale-up" data-trigger="hover">
                                <a class="img-box"><img class="anima" src="<?php echo $key['img_url'] ?>" alt="" /></a>
                                <div class="advs-box-content">
                                    <h3 class="<?php echo strlen($key['title_cp_services']) > 15 ? 'text-m' : '' ?>"><?php echo $key['title_cp_services'] ?></h3>
                                    <p class="text-justify">
                                        <?php echo $key['desc_cp_services'] ?>
                                    </p>
                                </div>
                            </div>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="section-bg-image parallax-window white" id="mn-contact" data-natural-height="1080" data-natural-width="1920" data-parallax="scroll" data-image-src="<?php echo $setting['home_contact_img'] ?>">
        <div class="container content">
            <div class="row">
                <div class="col-md-6 boxed" data-anima="fade-left">
                    <h2 class="text-color">Find Us Here</h2>
                    <hr class="space s" />
                    <p class="text-s">
                        Jl. Raya By Pass Jomin No. 88 <br />
                        Cikampek - Jawa Barat<br />
                        (41373)<br />
                    </p>
                    <hr class="space s" />
                    <div class="mapouter">
                        <div class="google-map">
                            <iframe width="100%" height="100%" id="google-map" src="https://maps.google.com/maps?q=pt%20prima%20usaha%20mitra%20abadi&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 boxed" data-anima="fade-right">
                    <h2 class="text-color">Contact Us</h2>
                    <div class="row">
                        <div class="col-lg-1">
                            <i class="im-mail text-l text-color"></i>
                        </div>
                        <div class="col-lg-7">
                            <span class="text-m"><i class="text-color">Don't hesitate to contact us</i></span>
                        </div>
                    </div>
                    <p class="text-s">
                        <b>Ready for offers and cooperation</b><br />
                        Phone: <?php echo $contact['cp_phone'] ?><br />
                        Email: <?php echo $contact['cp_email'] ?><br />
                    </p>
                    <hr class="space m" />
                    <form action="<?php echo base_url('message/send') ?>" class="form-box form-ajax" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Name</p>
                                <input id="name" name="name" placeholder="name" type="text" class="form-control form-value" required="">
                            </div>
                            <div class="col-md-6">
                                <p>Email</p>
                                <input id="email" name="email" placeholder="email" type="email" class="form-control form-value" required="">
                            </div>
                        </div>
                        <hr class="space xs">
                        <p>Phone</p>
                        <input id="phone" name="phone" placeholder="phone" type="text" class="form-control form-value">
                        <hr class="space xs">
                        <p>Message</p>
                        <textarea id="message" name="message" class="form-control form-value" required=""></textarea>
                        <hr class="space s">
                        <button class="btn-sm btn" type="submit">Send message</button>
                        <div class="success-box">
                            <div class="alert alert-success">Your message has been sent successfully, We'll respond it quickly</div>
                        </div>
                        <div class="error-box">
                            <div class="alert alert-warning">Error, please retry. Your message has not been sent</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty bg-white">
        <div class="container content">
            <table width="100%">
                <tbody>
                    <tr>
                        <td class="text-center"><img src="<?php echo base_url() ?>assets/images/vendor1.jpg" alt="" style="height:200px"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <i class="scroll-top scroll-top-mobile show fa fa-sort-asc" id="scrolltotop"></i>
    <footer class="footer-base">
        <div class="container content">
            <div class="row">
                <div class="col-md-4">
                    <img class="logo" src="<?php echo base_url() ?>assets/images/logopumabaru.png" alt="logo" style="height: 100px" />
                    <p class="text-s">
                        PT Prima Usaha Mandiri Abadi
                    </p>
                    <hr class="space s" />
                    <div class="btn-group social-group btn-group-icons">
                        <a target="_blank" href="<?php echo $setting['link1'] ?>" data-toggle="tooltip" data-placement="top" title="Youtube">
                            <i class="fa fa-youtube text-s circle"></i>
                        </a>
                        <a target="_blank" href="<?php echo $setting['link2'] ?>" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                            <i class="fa fa-linkedin text-s circle"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <h3 class="text-black text-uppercase">Contact Info</h3>
                    <hr class="space space-30" />
                    <ul class="fa-ul text-s ul-squares">
                        <li><?php echo $contact['cp_addr1'] ?></li>
                        <li><?php echo $contact['cp_addr2'] ?></li>
                        <li><?php echo $contact['cp_email'] ?></li>
                        <li><?php echo $contact['cp_phone'] ?></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3 class="text-black text-uppercase">Useful resources</h3>
                    <hr class="space space-30" />
                    <!-- Histats.com  (div with counter) -->
                    <div id="histats_counter"></div>
                    <!-- Histats.com  START  (aync)-->
                    <script type="text/javascript">
                        var _Hasync = _Hasync || [];
                        _Hasync.push(['Histats.start', '1,4367222,4,203,118,65,00001010']);
                        _Hasync.push(['Histats.fasi', '1']);
                        _Hasync.push(['Histats.track_hits', '']);
                        (function() {
                            var hs = document.createElement('script');
                            hs.type = 'text/javascript';
                            hs.async = true;
                            hs.src = ('//s10.histats.com/js15_as.js');
                            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
                        })();
                    </script>
                    <noscript><a href="/" target="_blank"><img src="//sstatic1.histats.com/0.gif?4367222&101" alt="counter free hit invisible" border="0"></a></noscript>
                    <!-- Histats.com  END  -->
                </div>
            </div>
            <hr class="space hidden-sm" />
            <div class="row copy-row">
                <div class="col-md-12 copy-text">
                    © 2020 PUMA
                </div>
            </div>
        </div>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/flexslider/jquery.flexslider-min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.tab-accordion.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/parallax.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/script.js"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/scripts/iconsmind/line-icons.min.css">
        <script src="<?php echo base_url() ?>assets/scripts/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/imagesloaded.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.twbsPagination.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/flexslider/jquery.flexslider-min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/jquery.tab-accordion.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/isotope.min.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/bootstrap/js/bootstrap.popover.min.js"></script>
        <script src='<?php echo base_url() ?>assets/scripts/php/contact-form.js'></script>
        <script src='<?php echo base_url() ?>assets/scripts/jquery.progress-counter.js'></script>
        <script src="<?php echo base_url() ?>assets/scripts/smooth.scroll.min.js"></script>
    </footer>
</body>

<!-- Mirrored from templates.framework-y.com/lightwire/pages/index-worker.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 07 Feb 2020 08:16:07 GMT -->

</html>