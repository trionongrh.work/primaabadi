<?php

class M_product extends CI_Model
{
    public function getProductCategory()
    {
        $this->db->select('*, CONCAT("filter-",id_product_category) as key_filter');
        $query = $this->db->get('product_category');
        return $query->result_array();
    }
    public function getProductCategoryItem($id_category)
    {
        $this->db->where('id_product_category', $id_category);
        $query = $this->db->get('product_category');
        return $query->row_array();
    }
    public function addProductCategory($data)
    {
        return $this->db->insert('product_category', $data);
    }
    public function deleteProductCategory($id_category)
    {
        $this->db->where('id_product_category', $id_category);
        return $this->db->delete('product_category');
    }
    public function editProductCategory($data)
    {
        $this->db->where('id_product_category', $data['id_product_category']);
        return $this->db->update('product_category', $data);
    }

    public function getProduct()
    {
        $this->db->select('*,CONCAT("' . base_url('uploads/product/') . '", image_product) as img_url');
        $product = $this->db->get('product')->result_array();
        $this->db->select('id_product,product_category.id_product_category,nama_product_category, CONCAT("filter-",product_category.id_product_category) as key_filter');
        $this->db->join('product_category', 'product_category.id_product_category=product_category_relations.id_product_category');
        $category = $this->db->get('product_category_relations')->result_array();
        $product_category = [];
        foreach ($category as $key) {
            $product_category[$key['id_product']][] = $key;
        }
        foreach ($product as $i => $key) {
            $product[$i]['category'] = $product_category[$key['id_product']];
        }

        return $product;
    }

    public function getProductNext($id,$cat)
    {
        $this->db->where('id_product = (select min(id_product) from product_category_relations where id_product > '.$id.' and id_product_category = '.$cat.' )');
        $product = $this->db->get('product')->row_array();
        return isset($product) ? $product['id_product'] : null;
    }
    
    public function getProductPrev($id,$cat)
    {
        $this->db->where('id_product = (select max(id_product) from product_category_relations where id_product < '.$id.' and id_product_category = '.$cat.' )');
        $product = $this->db->get('product')->row_array();
        return isset($product) ? $product['id_product'] : null;
    }

    public function getCategoryWithProduct()
    {
        $this->db->select('*, CONCAT("filter-",id_product_category) as key_filter');
        $category = $this->db->get('product_category')->result_array();

        $this->db->select('product.*,CONCAT("' . base_url('uploads/product/') . '", image_product) as img_url, product_category_relations.id_product_category');
        $this->db->join('product_category_relations', 'product_category_relations.id_product=product.id_product');
        $this->db->order_by('product.id_product','ASC');
        $product = $this->db->get('product')->result_array();

        $pc = [];
        foreach ($product as $key) {
            $pc[$key['id_product_category']][] = $key;
        }
        foreach ($category as $i => $key) {
            $category[$i]['product'] = isset($pc[$key['id_product_category']]) ? $pc[$key['id_product_category']] : [];
        }

        return $category;
    }

    public function getProductOne($id_product)
    {
        $this->db->select('*,CONCAT("' . base_url('uploads/product/') . '", image_product) as img_url');
        $this->db->where('id_product', $id_product);
        $product = $this->db->get('product')->row_array();
        $this->db->where('id_product', $id_product);
        $this->db->select('id_product,product_category.id_product_category,nama_product_category, CONCAT("filter-",product_category.id_product_category) as key_filter');
        $this->db->join('product_category', 'product_category.id_product_category=product_category_relations.id_product_category');
        $category = $this->db->get('product_category_relations')->result_array();
        $product_category = [];
        foreach ($category as $key) {
            $product_category[$key['id_product']][] = $key;
        }
        $product['category'] = $product_category[$key['id_product']];

        return $product;
    }
    public function addProduct($data)
    {
        return $this->db->insert('product', $data);
    }
    public function addProductId()
    {
        return $this->db->insert_id();
    }
    public function addProductCategortRelations($data)
    {
        return $this->db->insert_batch('product_category_relations', $data);
    }

    public function editProduct($data_product, $data_category)
    {
        $this->db->where('id_product', $data_product['id_product']);
        $product = $this->db->update('product', $data_product);

        $this->db->where('id_product', $data_product['id_product']);
        $this->db->delete('product_category_relations');

        $category = $this->db->insert_batch('product_category_relations', $data_category);

        return $product && $category;
    }

    public function deleteProduct($id_product)
    {
        $this->db->where('id_product', $id_product);
        $cat =  $this->db->delete('product_category_relations');
        $this->db->where('id_product', $id_product);
        $prod =  $this->db->delete('product');
        return $cat && $prod;
    }
}
