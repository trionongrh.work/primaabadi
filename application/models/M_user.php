<?php

class M_user extends CI_Model {
    const TABLE_NAME = 'user';
    const TABLE_LEVEL = 'user_level';
    const TABLE_LOG = 'user_log';

    public function getLogin($username, $password){
        // $this->db->select('username,nama_user,user.user_level, name_level');
        $this->db->join('user_level', 'user_level.user_level=user.user_level');
        $this->db->where('username', $username);
        $this->db->where('password', md5($password));
        $query = $this->db->get('user');
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return false;
        }
    }

    
}