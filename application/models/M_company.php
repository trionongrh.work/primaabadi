<?php

class M_company extends CI_Model
{

    public function getVisi()
    {
        $this->db->where('key', 'cp_visi');
        $query = $this->db->get('company_profile');
        return $query->row_array()['value'];
    }
    public function getMisi()
    {
        $this->db->where('key', 'cp_misi');
        $query = $this->db->get('company_profile');
        return $query->row_array()['value'];
    }
    public function getContact()
    {
        $this->db->where_in('key', ['cp_phone', 'cp_email', 'cp_addr1', 'cp_addr2']);
        $query = $this->db->get('company_profile');
        $contact = $query->result_array();
        foreach ($contact as $key) {
            $output[$key['key']] = $key['value'];
        }
        return $output;
    }
    public function getLink()
    {
        $this->db->where_in('key', ['cp_link_1', 'cp_link_2']);
        $query = $this->db->get('company_profile');
        $contact = $query->result_array();
        foreach ($contact as $key) {
            $output[$key['key']] = $key['value'];
        }
        return $output;
    }
    public function editContact($data)
    {
        $query = $this->db->update_batch('company_profile', $data, 'key');
        return $query;
    }
    public function editVisi($value)
    {
        $data = array('value' => $value);
        $this->db->where('key', 'cp_visi');
        $query = $this->db->update('company_profile', $data);
        return $query;
    }
    public function editMisi($value)
    {
        $data = array('value' => $value);
        $this->db->where('key', 'cp_misi');
        $query = $this->db->update('company_profile', $data);
        return $query;
    }
    public function getServices()
    {
        $this->db->select('*, CONCAT("' . base_url('uploads/services/') . '", image_cp_services) as img_url');
        $query = $this->db->get('company_profile_services');
        return $query->result_array();
    }
    public function getService($id_service)
    {
        $this->db->select('*, CONCAT("' . base_url('uploads/services/') . '", image_cp_services) as img_url');
        $this->db->where('id_cp_services', $id_service);
        $query = $this->db->get('company_profile_services');
        return $query->row_array();
    }
    public function getServicesNum()
    {
        $query = $this->db->get('company_profile_services');
        return $query->num_rows();
    }
    public function addServices($data)
    {
        return $this->db->insert('company_profile_services', $data);
    }
    public function editServices($data)
    {
        $this->db->where('id_cp_services', $data['id_cp_services']);
        return $this->db->update('company_profile_services', $data);
    }
    public function deleteServices($id_service)
    {
        $this->db->where('id_cp_services', $id_service);
        return $this->db->delete('company_profile_services');
    }
    public function getAboutList()
    {
        $this->db->select('*, CONCAT("' . base_url('uploads/about/') . '", image_cp_about) as img_url');
        $query = $this->db->get('company_profile_aboutlist');
        return $query->result_array();
    }
    public function getAboutListItem($id_about)
    {
        $this->db->select('*, CONCAT("' . base_url('uploads/about/') . '", image_cp_about) as img_url');
        $this->db->where('id_cp_about', $id_about);
        $query = $this->db->get('company_profile_aboutlist');
        return $query->row_array();
    }
    public function addAboutList($data)
    {
        return $this->db->insert('company_profile_aboutlist', $data);
    }
    public function editAboutList($data)
    {
        $this->db->where('id_cp_about', $data['id_cp_about']);
        return $this->db->update('company_profile_aboutlist', $data);
    }
    public function deleteAboutList($id_about)
    {
        $this->db->where('id_cp_about', $id_about);
        return $this->db->delete('company_profile_aboutlist');
    }
    public function getAboutDetail()
    {
        $this->db->where_in('key', ['cp_about_title', 'cp_about_line1', 'cp_about_line2', 'cp_about_image']);
        $query = $this->db->get('company_profile');
        $about = $query->result_array();
        foreach ($about as $key) {
            $output[$key['key']] = $key['value'];
        }
        return $output;
    }
    public function addSlider($data)
    {
        return $this->db->insert('company_profile_slider', $data);
    }
    public function getSlider()
    {
        $this->db->select('*, CONCAT("' . base_url('uploads/slider/') . '", image_slider) as img_url');
        $query = $this->db->get('company_profile_slider');
        return $query->result_array();
    }
    public function getSliderId($id)
    {
        $this->db->select('*, CONCAT("' . base_url('uploads/slider/') . '", image_slider) as img_url');
        $this->db->where('id_cp_slider', $id);
        $query = $this->db->get('company_profile_slider');
        return $query->row_array();
    }
    public function deleteSlider($id)
    {
        $this->db->where('id_cp_slider',$id);
        return $this->db->delete('company_profile_slider');
    }
    public function getSliderConfig()
    {
        $query = $this->db->get('company_profile_slider_config');
        $about = $query->result_array();
        foreach ($about as $key) {
            $output[$key['key']] = $key['value'];
        }
        return $output;
    }
    public function setSliderConfig($data)
    {
        return $this->db->update_batch('company_profile_slider_config', $data, 'key');
    }
    public function editCompany($data)
    {
        $query = $this->db->update_batch('company_profile', $data, 'key');
        return $query;
    }
}
