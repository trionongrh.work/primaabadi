<?php


class M_mail extends CI_Model{

    public function getMail(){
        $this->db->order_by('at','DESC');
        $query = $this->db->get('inbox');
        return $query->result_array();
    }

    
    public function getMailDetail($id_mail){
        $this->db->where('id_inbox',$id_mail);
        $query = $this->db->get('inbox');
        return $query->row_array();
    }
    
    public function getMailSize(){
        $query = $this->db->get('inbox');
        return $query->num_rows();
    }

    
    public function getMailSizeUnread(){
        $this->db->where('is_read',0);
        $query = $this->db->get('inbox');
        return $query->num_rows();
    }

    public function addMail($data){
        return $this->db->insert('inbox',$data);
    }

    public function deleteMail($id_mail){
        $this->db->where('id_inbox',$id_mail);
        return $this->db->delete('inbox');
    }

    public function setRead($id_mail){
        $this->db->where('id_inbox', $id_mail);
        return $this->db->update('inbox',array('is_read' => 1));
    }
}