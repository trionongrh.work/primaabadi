<?php


class M_settings extends CI_Model
{

    public function getMaintenance()
    {
        $this->db->where('config_key', 'maintenanceMode');
        $query = $this->db->get('config');
        return $query->row_array()['value'];
    }

    public function getSettings($plain = FALSE)
    {
        $settings = $this->db->get('config')->result_array();
        $output = [];
        foreach ($settings as $i => $key) {
            $split = explode('_', $key['config_key']);
            if (isset($split[2]) && $split[2] == 'img') {
                if ($plain) {
                    $output[$key['config_key']] = $key['value'];
                } else {
                    $output[$key['config_key']] = base_url('uploads/company/') . $key['value'];
                }
            } else {
                $output[$key['config_key']] = $key['value'];
            }
        }
        return $output;
    }

    public function setConfig($data)
    {
        return $this->db->update_batch('config', $data, 'config_key');
    }
}
